/*
mpcx_neg

computes h = -f

Copyright (C) 2009 Andreas Enge

This file is part of the MPFRCX Library.

The MPFRCX Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The MPFRCX Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MPFRCX library; see the file COPYING.LESSER.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
*/

#include "mpfrcx-impl.h"

void mpcx_neg (mpcx_ptr h, mpcx_srcptr f) {
   int i;

   h->deg = f->deg;
   if (h->size < h->deg + 1)
      mpcx_realloc (h, h->deg + 1);

   for (i = h->deg; i >= 0; i--)
      mpc_neg (h->coeff [i], f->coeff [i], MPC_RNDNN);
}
