/*
mpfrx_init_set

initialises h with the precision of f and copies f to h
necessary

Copyright (C) 2009 Andreas Enge

This file is part of the MPFRCX Library.

The MPFRCX Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The MPFRCX Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MPFRCX library; see the file COPYING.LESSER.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
*/

#include "mpfrcx-impl.h"

void mpfrx_init_set (mpfrx_ptr h, mpfrx_srcptr f) {
   int i;

   mpfrx_init (h, f->deg + 1, f->prec);

   h->deg = f->deg;
   for (i = 0; i <= f->deg; i++)
      mpfr_set (h->coeff [i], f->coeff [i], GMP_RNDN);
}
