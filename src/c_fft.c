/*
MPFRCX_FFT

functions for the complex fft and multiplication using it

Copyright (C) 2009, 2010, 2012 Andreas Enge

This file is part of the MPFRCX Library.

The MPFRCX Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The MPFRCX Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MPFRCX library; see the file COPYING.LESSER.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
*/

#include "mpfrcx-impl.h"

#define MPFRCX_FFTRB  1
#define MPFRCX_FFTREV 2
#define MPFRCX_FFT    3

static void mpcx_dft (mpc_t *f, int n, mpc_t *zeta, int zeta_off,
                      int direction);
static void mpcx_dftrev (mpc_t *f, int n, mpc_t *zeta, int zeta_off,
                         mpc_ptr tmp);
static void mpcx_dftrev_inv (mpc_t *f, int n, mpc_t *zeta, int zeta_off,
                             mpc_ptr tmp);

/**************************************************************************/

void mpcx_zeta_init (mpc_t **zeta, int n, mpfr_prec_t prec) {
   /* Computes the n/2 first powers of the n-th root of unity (except for */
   /* 1 and i) and stores them into zeta.                                 */
   /* zeta is initially a null pointer and is allocated by the function.  */
   /* n must be divisible by 4.                                           */

   int   i;
   mpc_t tmp;

   mpc_init2 (tmp, prec);
   *zeta = (mpc_t *) malloc (n/2 * sizeof (mpc_t));
   for (i = 0; i < n/2; i++)
      mpc_init2 ((*zeta) [i], prec);

   mpfr_const_pi (tmp->re, GMP_RNDN);
   mpfr_mul_2ui (tmp->re, tmp->re, 1, GMP_RNDN);
   mpfr_div_ui (tmp->re, tmp->re, n, GMP_RNDN);
   mpfr_sin_cos ((*zeta) [1]->im, (*zeta) [1]->re, tmp->re, GMP_RNDN);
   for (i = 2; i < n/4; i++)
      mpc_mul ((*zeta) [i], (*zeta) [i-1], (*zeta) [1], MPC_RNDNN);
   for (i = n/4 + 1; i < n/2; i++) {
      mpfr_neg ((*zeta) [i]->re, (*zeta) [n/2 - i]->re, GMP_RNDN);
      mpfr_set ((*zeta) [i]->im, (*zeta) [n/2 - i]->im, GMP_RNDN);
   }

   mpc_clear (tmp);
}

/**************************************************************************/

void mpcx_zeta_clear (mpc_t *zeta, int n) {
   /* clears the variable allocated by mpcx_zeta_init                    */
   int i;

   for (i = 0; i < n/2; i++)
      mpc_clear (zeta [i]);
   free (zeta);
}

/**************************************************************************/

void mpcx_twiddle_init (mpfr_t **twiddle, int n, mpfr_prec_t prec) {
   /* Computes the twiddle factors (1 / 2 * Re (zeta^k)) needed for the   */
   /* Rader-Brenner-FFT and stores them in twiddle.                       */
   /* twiddle is initially a null pointer and is allocated by the         */
   /* function                                                            */
   /* n must be divisible by 4.                                           */
   int   i;
   mpc_t *zeta;

   mpcx_zeta_init (&zeta, n, prec);

   *twiddle = (mpfr_t *) malloc (n/2 * sizeof (mpfr_t));
   for (i = 0; i < n/2; i++)
      mpfr_init2 ((*twiddle) [i], prec);

   mpfr_ui_div ((*twiddle) [1], 1, zeta [1]->re, GMP_RNDN);
   mpfr_div_2ui ((*twiddle) [1], (*twiddle) [1], 1, GMP_RNDN);
   for (i = 2; i < n/4; i++) {
      mpfr_ui_div ((*twiddle) [i], 1, zeta [i]->re, GMP_RNDN);
      mpfr_div_2ui ((*twiddle) [i], (*twiddle) [i], 1, GMP_RNDN);
   }
   for (i = n/4 + 1; i < n/2; i++)
      mpfr_neg ((*twiddle) [i], (*twiddle) [n/2 - i], GMP_RNDN);

   mpcx_zeta_clear (zeta, n);
}

/**************************************************************************/

void mpcx_twiddle_clear (mpfr_t *twiddle, int n)
   /* clears the variable allocated by mpcx_twiddle_init                  */
{
   int i;

   for (i = 0; i < n/2; i++)
      mpfr_clear (twiddle [i]);
   free (twiddle);
}

/**************************************************************************/

static void mpcx_dft (mpc_t *f, int n, mpc_t *zeta, int zeta_off,
                      int direction) {
   /* Replaces f by its DFT of order n (if direction >= 0) or n times its */
   /* inverse DFT (if direction < 0).                                     */
   /* We perform a decimation in frequency, splitting f into a left and a */
   /* right block. In fact, this corrresponds exactly to the algorithm in */
   /* [GG99].                                                             */
   /* n must be a power of 2, and f must contain exactly n coefficients.  */
   /* zeta [i*zeta_off] contains the i-th power of the n-th root of unity.*/

   int i;
   mpc_t *F, tmp;

   if (n > 1) {
      F = (mpc_t*) malloc (n * sizeof (mpc_t));
      for (i = 0; i < n; i++)
         mpc_init2 (F [i], mpc_get_prec (f [0]));
      mpc_init2 (tmp, mpc_get_prec (f [0]));

      /* Let f = f0 + X^(n/2) f1; let F0 be f0+f1 and let F1 be f0-f1,    */
      /* multiplied by powers of zeta ("butterfly operation").            */
      /* To avoid the multiplication by 1, it is treated before the loop. */
      mpc_add (F [0], f [0], f [n/2], MPC_RNDNN);
      mpc_sub (F [n/2], f [0], f [n/2], MPC_RNDNN);
      for (i = 1; i < n/2; i++) {
         mpc_add (F [i], f [i], f [n/2 + i], MPC_RNDNN);
         mpc_sub (F [n/2 + i], f [i], f [n/2 + i], MPC_RNDNN);
         /* To speed up multiplication by i, check for this case. */
         if (i == n/4)
            mpc_mul_i (F [3*n/4], F [3*n/4], direction, MPC_RNDNN);
         else
         {
            if (direction < 0)
            {
               mpc_conj (tmp, zeta [i*zeta_off], MPC_RNDNN);
               mpc_mul (F [n/2 + i], F [n/2 + i], tmp, MPC_RNDNN);
            }
            else
               mpc_mul (F [n/2 + i], F [n/2 + i], zeta [i*zeta_off],
                        MPC_RNDNN);
         }
      }

      /* Now recurse. */
      mpcx_dft (F, n/2, zeta, 2*zeta_off, direction);
      mpcx_dft (F+n/2, n/2, zeta, 2*zeta_off, direction);

      /* Reorder the result into f. */
      for (i = 0; i < n/2; i++) {
         mpc_set (f [2*i], F [i], MPC_RNDNN);
         mpc_set (f [2*i+1], F [n/2 + i], MPC_RNDNN);
      }

      for (i = 0; i < n; i++)
         mpc_clear (F [i]);
      free (F);
      mpc_clear (tmp);
   }
}

/**************************************************************************/

void mpcx_dftrb (mpc_t *f, int n, mpfr_t *twiddle,
                        int zeta_off, int direction) {
   /* Replaces f by its DFT of order n (if direction >= 0) or n times its */
   /* inverse DFT (if direction < 0), using the Rader-Brenner algorithm,  */
   /* see Chapter 4.3 in Nussbaumer.                                      */
   /* n must be a power of 2, and f must contain exactly n coefficients.  */
   /* twiddle [i*zeta_off] contains 1 / 2 Re (zeta [i*zeta_off]).         */

   int i;
   mpc_t *F, tmp, v;

   if (n == 2) {
      mpc_init2 (tmp, mpc_get_prec (f [0]));
      mpc_sub (tmp, f [0], f [1], MPC_RNDNN);
      mpc_add (f [0], f [0], f [1], MPC_RNDNN);
      mpc_set (f [1], tmp, MPC_RNDNN);
      mpc_clear (tmp);
   }
   else if (n > 2) {
      F = (mpc_t*) malloc (n * sizeof (mpc_t));
      for (i = 0; i < n; i++)
         mpc_init2 (F [i], mpc_get_prec (f [0]));
      mpc_init2 (v, mpc_get_prec (f [0]));

      /* Let f = f0 + X^(n/2) f1; let F0 be f0+f1 and let F1 be f0-f1,    */
      /* multiplied by inverses of traces of powers of zeta.              */
      /* To avoid the multiplication by 1, it is treated before the loop. */
      mpc_add (F [0], f [0], f [n/2], MPC_RNDNN);
      mpc_sub (F [n/2], f [0], f [n/2], MPC_RNDNN);
      mpc_div_2ui (F [n/2], F [n/2], 1, MPC_RNDNN);
      for (i = 1; i < n/2; i++) {
         mpc_add (F [i], f [i], f [n/2 + i], MPC_RNDNN);
         if (i == n/4) {
            mpc_set_ui (F [3*n/4], 0, MPC_RNDNN);
            mpc_sub (v, f [n/4], f [3*n/4], MPC_RNDNN);
            mpc_mul_i (v, v, direction, MPC_RNDNN);
         }
         else {
            mpc_sub (F [n/2 + i], f [i], f [n/2 + i], MPC_RNDNN);
            mpc_mul_fr (F [n/2 + i], F [n/2 + i], twiddle [i*zeta_off],
               MPC_RNDNN);
         }
      }

      /* Now recurse. */
      mpcx_dftrb (F, n/2, twiddle, 2*zeta_off, direction);
      mpcx_dftrb (F+n/2, n/2, twiddle, 2*zeta_off, direction);

      /* Construct the result in f. */
      for (i = 0; i < n/2 - 1; i++) {
         mpc_set (f [2*i], F [i], MPC_RNDNN);
         mpc_add (f [2*i+1], F [n/2 + i], F [n/2 + i+1], MPC_RNDNN);
         if (i % 2 == 0)
            mpc_add (f [2*i+1], f [2*i+1], v, MPC_RNDNN);
         else
            mpc_sub (f [2*i+1], f [2*i+1], v, MPC_RNDNN);
      }
      mpc_set (f [n-2], F [n/2 - 1], MPC_RNDNN);
      mpc_add (f [n-1], F [n-1], F [n/2], MPC_RNDNN);
      mpc_sub (f [n-1], f [n-1], v, MPC_RNDNN);

      for (i = 0; i < n; i++)
         mpc_clear (F [i]);
      free (F);
      mpc_clear (v);
   }
}

/*****************************************************************************/

static void mpcx_dftrev (mpc_t *f, int n, mpc_t *zeta, int zeta_off,
                         mpc_ptr tmp) {
   /* Replaces f by its DFT of order n in reversed bit order. Otherwise   */
   /* said, let phi_n be the function on {0,...n-1} that on input i       */
   /* returns the integer obtained from writing i with log_2 (n) bits     */
   /* and reading it from right to left. Then f [i] is replaced by        */
   /* DFT (f) [phi_n (i)].                                                */
   /* We perform a decimation in frequency, splitting f into a left and a */
   /* right block. In fact, this corrresponds exactly to the algorithm in */
   /* [[GG99], but omitting the permutation in the end.                   */
   /* This means that f is mostly scanned from left to right (starting    */
   /* with the leftmost and the middle entry simultaneously). Also the    */
   /* roots of  unity are scanned in the natural order. Consequently, in  */
   /* the recursion steps, stepping through the roots of unity will be by */
   /* powers of 2.                                                        */
   /* Thus, we are cache-friendly with respect to f, but not with respect */
   /* to the roots of unity.                                              */
   /* n must be a power of 2, and f must contain exactly n coefficients.  */
   /* zeta [i*zeta_off] contains the i-th power of the n-th root of unity.*/
   /* tmp is a temporary variable, just passed through to avoid recursive */
   /* allocation.                                                         */

   int i;

   if (n > 1) {
      /* Let f = f0 + X^(n/2) f1; replace f0 by f0+f1 and f1 by f0-f1,    */
      /* multiplied by powers of zeta ("butterfly operation").            */
      /* To avoid the multiplication by 1, it is treated before the loop. */
      mpc_sub (tmp, f [0], f [n/2], MPC_RNDNN);
      mpc_add (f [0], f [0], f [n/2], MPC_RNDNN);
      mpc_set (f [n/2], tmp, MPC_RNDNN);
      for (i = 1; i < n/2; i++) {
         mpc_sub (tmp, f [i], f [n/2 + i], MPC_RNDNN);
         mpc_add (f [i], f [i], f [n/2 + i], MPC_RNDNN);
         /* To speed up multiplication by i, check for this case. */
         if (i == n/4)
            mpc_mul_i (f [3*n/4], tmp, 1, MPC_RNDNN);
         else
            mpc_mul (f [n/2 + i], tmp, zeta [i*zeta_off], MPC_RNDNN);
      }

      /* Now recurse.                                                     */
      mpcx_dftrev (f, n/2, zeta, 2*zeta_off, tmp);
      mpcx_dftrev (f+n/2, n/2, zeta, 2*zeta_off, tmp);
   }
}

/**************************************************************************/

static void mpcx_dftrev_inv (mpc_t *f, int n, mpc_t *zeta, int zeta_off,
                             mpc_ptr tmp) {
   /* Replaces f by n times its inverse DFT of order n (computed with the */
   /* inverse root of unity). It is assumed that the input is in reversed */
   /* bit order; the output is in normal bit order. So up to a factor n,  */
   /* this function is exactly the inverse of the previous one.           */
   /* Again, we perform a decimation in frequency.                        */
   /* n must be a power of 2, and f must contain exactly n coefficients.  */
   /* zeta [i*zeta_off] contains the i-th power of the n-th root of       */
   /* unity                                                               */
   /* tmp is a temporary variable, just passed through to avoid recursive */
   /* allocation.                                                         */

   int i;

   if (n > 1)
   {
      /* First recurse.                                                   */
      mpcx_dftrev_inv (f, n/2, zeta, 2*zeta_off, tmp);
      mpcx_dftrev_inv (f+n/2, n/2, zeta, 2*zeta_off, tmp);

      /* Then do a butterfly operation between the left and the right     */
      /* half. This time, first multiply the right half by powers of      */
      /* zeta, then perform the addition/subtraction crossing.            */
      /* To avoid the multiplication by 1, it is treated before the loop. */
      mpc_sub (tmp, f [0], f [n/2], MPC_RNDNN);
      mpc_add (f [0], f [0], f [n/2], MPC_RNDNN);
      mpc_set (f [n/2], tmp, MPC_RNDNN);
      for (i = 1; i < n/2; i++) {
         /* To speed up multiplication by -i, check for this case. */
         if (i == n/4)
            mpc_mul_i (f [3*n/4], f[3*n/4], -1, MPC_RNDNN);
         else {
            mpc_conj (tmp, zeta [i*zeta_off], MPC_RNDNN);
            mpc_mul (f [n/2 + i], f [n/2 + i], tmp, MPC_RNDNN);
         }
         mpc_sub (tmp, f [i], f [n/2 + i], MPC_RNDNN);
         mpc_add (f [i], f [i], f [n/2 + i], MPC_RNDNN);
         mpc_set (f [n/2 + i], tmp, MPC_RNDNN);
      }
   }
}

/*****************************************************************************/

void mpcx_array_mul_fft (mpc_t *h, mpc_t *f, mpc_t *g, int m, int n) {
   /* Replace h by the product of f and g, using the FFT.                 */
   /* f must have m, g must have n coefficients, and h must have          */
   /* sufficiently many coefficients to hold the result.                  */

   int    i;
   mpc_t  tmp, *zeta;
   mpfr_t *twiddle;
   mpc_t  *F, *G, *H;
      /* holds the FFTs of f, g and h */
   int    N;
      /* order of the FFT */
   int    method = MPFRCX_FFTRB;
      /* method must be one of MPFRCX_FFT, MPFRCX_FFTREV or MPFRCX_FFTRB        */

   N = 4;
      /* necessary because we break the FFTs down to the level of fourth  */
      /* roots of unity                                                   */
   while (N < m + n - 1)
      N *= 2;

   mpc_init2 (tmp, mpc_get_prec (h [0]));
   if (method == MPFRCX_FFTRB)
      mpcx_twiddle_init (&twiddle, N, mpc_get_prec (h [0]));
   else
      mpcx_zeta_init (&zeta, N, mpc_get_prec (h [0]));
   F = (mpc_t *) malloc (N * sizeof (mpc_t));
   G = (mpc_t *) malloc (N * sizeof (mpc_t));
   H = (mpc_t *) malloc (N * sizeof (mpc_t));
   for (i = 0; i < N; i++) {
      mpc_init2 (F [i], mpc_get_prec (h [0]));
      mpc_init2 (G [i], mpc_get_prec (h [0]));
      mpc_init2 (H [i], mpc_get_prec (h [0]));
   }

   /* Copy f into F and g into G and perform the FFTs. */
   for (i = 0; i < m; i++)
      mpc_set (F [i], f [i], MPC_RNDNN);
   for (i = m; i < N; i++)
      mpc_set_ui (F [i], 0, MPC_RNDNN);
   if (method == MPFRCX_FFT)
      mpcx_dft (F, N, zeta, 1, 1);
   else if (method == MPFRCX_FFTREV)
      mpcx_dftrev (F, N, zeta, 1, tmp);
   else
      mpcx_dftrb (F, N, twiddle, 1, 1);

   for (i = 0; i < n; i++)
      mpc_set (G [i], g [i], MPC_RNDNN);
   for (i = n; i < N; i++)
      mpc_set_ui (G [i], 0, MPC_RNDNN);
   if (method == MPFRCX_FFT)
      mpcx_dft (G, N, zeta, 1, 1);
   else if (method == MPFRCX_FFTREV)
      mpcx_dftrev (G, N, zeta, 1, tmp);
   else
      mpcx_dftrb (G, N, twiddle, 1, 1);

   /* Perform the pointwise multiplication and the inverse                */
   /* transformation.                                                     */
   /* Comment: to save memory, one should multiply into F, not into H     */
   for (i = 0; i < N; i++)
      mpc_mul (H [i], F [i], G [i], MPC_RNDNN);
   if (method == MPFRCX_FFT)
      mpcx_dft (H, N, zeta, 1, -1);
   else if (method == MPFRCX_FFTREV)
      mpcx_dftrev_inv (H, N, zeta, 1, tmp);
   else
      mpcx_dftrb (H, N, twiddle, 1, -1);
   for (i = 0; i < m + n - 1; i++)
      mpc_div_ui (h [i], H [i], N, MPC_RNDNN);

   mpc_clear (tmp);
   if (method == MPFRCX_FFTRB)
      mpcx_twiddle_clear (twiddle, N);
   else
      mpcx_zeta_clear (zeta, N);
   for (i = 0; i < N; i++) {
      mpc_clear (F [i]);
      mpc_clear (G [i]);
      mpc_clear (H [i]);
   }
   free (F);
   free (G);
   free (H);
}

/*****************************************************************************/
