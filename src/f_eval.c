/*
mpfrx_eval

computes f (x)

Copyright (C) 2020 Andreas Enge

This file is part of the MPFRCX Library.

The MPFRCX Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The MPFRCX Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MPFRCX library; see the file COPYING.LESSER.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
*/

#include "mpfrcx-impl.h"

void mpfrx_eval (mpfr_ptr rop, mpfrx_srcptr f, mpfr_srcptr x) {

   int overlap = rop == x;
   int deg = mpfrx_get_deg (f);
   int i;
   mpfr_t r;

   if (deg == -1)
      mpfr_set_ui (rop, 0, MPC_RNDNN);
   else if (deg == 0)
      mpfr_set (rop, mpfrx_get_coeff (f, 0), MPC_RNDNN);
   else {
      if (overlap)
         mpfr_init2 (r, mpfr_get_prec (rop));
      else
         r [0] = rop [0];

      mpfr_set (r, mpfrx_get_coeff (f, deg), MPC_RNDNN);
      for (i = deg - 1; i >= 0; i--)
         mpfr_fma (r, r, x, mpfrx_get_coeff (f, i), MPC_RNDNN);

      if (overlap)
         mpfr_clear (rop);
      rop [0] = r [0];
   }
}

