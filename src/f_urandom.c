/*
mpfrx_urandom

creates a random polynomial

Copyright (C) 2009 Andreas Enge

This file is part of the MPFRCX Library.

The MPFRCX Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The MPFRCX Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MPFRCX library; see the file COPYING.LESSER.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
*/

#include "mpfrcx-impl.h"

void mpfrx_urandom (mpfrx_ptr f, int deg, gmp_randstate_t state) {

   if (deg < 0)
      f->deg = -1;
   else {
      int i;
      f->deg = (int) gmp_urandomm_ui (state, deg + 1);
      if (f->size < f->deg + 1)
         mpfrx_realloc (f, f->deg + 1);
      for (i = f->deg; i >= 0; i--)
         mpfr_urandomb (f->coeff [i], state);
      while (!mpfr_cmp_ui (f->coeff [f->deg], 0))
         mpfr_urandomb (f->coeff [f->deg], state);
   }
}
