/*
mpfrcx.h

include file for polynomials over mpfr and mpc

Copyright (C) 2009, 2010, 2011, 2012, 2013, 2014, 2017, 2018, 2020, 2021 Andreas Enge

This file is part of the MPFRCX Library.

The MPFRCX Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The MPFRCX Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MPFRCX library; see the file COPYING.LESSER.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
*/

#ifndef __MPFRCX_H
#define __MPFRCX_H
#include <stdio.h>
#include <stdlib.h>
#include "gmp.h"
#include "mpfr.h"
#include "mpc.h"

/* Backwards compatibility with mpfr<3.0.0 (copied from mpc.h of mpc-0.8.3) */
#ifndef mpfr_exp_t
#define mpfr_exp_t mp_exp_t
#endif

#define MPFRCX_VERSION_MAJOR      0
#define MPFRCX_VERSION_MINOR      6
#define MPFRCX_VERSION_PATCHLEVEL 4
#define MPFRCX_VERSION_STRING     "0.6.4dev"

typedef struct {
   int size;
   /* the number of currently allocated coefficients. High level          */
   /* arithmetic functions may increase this number if more space is      */
   /* needed. Coefficients are only freed when the whole polynomial is.   */
   /* Allocated coefficients are immediately initialised with mpc_init.   */
   int   deg;
   /* A degree of -1 indicates the zero polynomial.                       */
   mpfr_prec_t prec;
   /* the precision used for initialising all the coefficients            */
   mpc_t *coeff;
}
__mpcx_struct;

typedef __mpcx_struct mpcx_t[1];
typedef __mpcx_struct *mpcx_ptr;
typedef const __mpcx_struct *mpcx_srcptr;

typedef struct {
   int    size;
   int   deg;
   mpfr_prec_t prec;
   mpfr_t *coeff;
}
__mpfrx_struct;

typedef __mpfrx_struct mpfrx_t[1];
typedef __mpfrx_struct *mpfrx_ptr;
typedef const __mpfrx_struct *mpfrx_srcptr;


typedef struct {
   int no;
   /* number of leaves */
   int levels;
   /* number of levels in the tree, equal to ceil (log_2 (no)) + 1 */
   mpcx_t** node;
   /* the tree content itself:                                            */
   /* - node [0,0], ..., node [0,no-1] are the leaves                     */
   /* - node [i+1,j] is the parent of node [i,2*j] and  node [i,2*j+1]    */
   /* If no is not a power of 2, then neither are the numbers of          */
   /* polynomials in each level.                                          */
   int* width;
   /* width [i] is the number of nodes in layer i                         */
}
__mpcx_tree_struct;

typedef __mpcx_tree_struct mpcx_tree_t [1];
typedef __mpcx_tree_struct *mpcx_tree_ptr;
typedef const __mpcx_tree_struct *mpcx_tree_srcptr;

typedef struct {
   int no;
   int levels;
   mpfrx_t** node;
   int* width;
}
__mpfrx_tree_struct;

typedef __mpfrx_tree_struct mpfrx_tree_t [1];
typedef __mpfrx_tree_struct *mpfrx_tree_ptr;
typedef const __mpfrx_tree_struct *mpfrx_tree_srcptr;

typedef struct {
   int levels;
   /* number of levels in the field tower decomposition */
   int* d;
   /* relative degree of each extension, starting from the bottom */
   int deg;
   /* total degree of the field extension */
   mpcx_t** W;
   /* W [i] for i=1, ..., levels-1 determines the field extension of
      degree d[i], stored in Hecke representation - if the absolute extension
      below W [i] has the minimal polynomial V_{i-1} in the variable X_{i-1},
      then W [i] stores the minimal polynomial of X_i, multiplied by V_{i-1}'.
      It is thus a (non-monic, with leading coefficient V_{i-1}') polynomial
      of degree d [i] in the variable X_i, the coefficients of which are
      polynomials in the variable X_{i-1}, stored in W [i][0], ...
      To be consistent with this, the final absolute extension of degree d [0]
      should be stored in W [0] as a polynomial in the variable X_0 with
      constant coefficients (that is, the coefficients are polynomials of
      degree 0...). To simplify, we store it as a univariate polynomial in
      W [0][0]. */
}
__mpcx_tower_struct;

typedef __mpcx_tower_struct mpcx_tower_t [1];
typedef __mpcx_tower_struct *mpcx_tower_ptr;
typedef const __mpcx_tower_struct *mpcx_tower_srcptr;

typedef struct {
   int levels;
   int* d;
   int deg;
   mpfrx_t** W;
}
__mpfrx_tower_struct;

typedef __mpfrx_tower_struct mpfrx_tower_t [1];
typedef __mpfrx_tower_struct *mpfrx_tower_ptr;
typedef const __mpfrx_tower_struct *mpfrx_tower_srcptr;


/* Check if stdio.h is included */
#if defined (_GMP_H_HAVE_FILE)
#define _MPFRCX_H_HAVE_FILE 1
#endif

#if defined (__cplusplus)
extern "C" {
#endif
extern const char *mpfrcx_get_version (void);

extern void mpcx_init (mpcx_ptr f, const int size, const mpfr_prec_t prec);
extern void mpcx_set (mpcx_ptr h, mpcx_srcptr f);
extern void mpcx_set_frx (mpcx_ptr h, mpfrx_srcptr f);
extern void mpcx_init_set (mpcx_ptr h, mpcx_srcptr f);
extern void mpcx_clear (mpcx_ptr f);
extern void mpcx_realloc (mpcx_ptr f, const int size);
extern void mpcx_swap (mpcx_ptr f, mpcx_ptr g);

extern void mpfrx_init (mpfrx_ptr f, const int size, const mpfr_prec_t prec);
extern void mpfrx_set (mpfrx_ptr h, mpfrx_srcptr f);
extern void mpfrx_init_set (mpfrx_ptr h, mpfrx_srcptr f);
extern void mpfrx_clear (mpfrx_ptr f);
extern void mpfrx_realloc (mpfrx_ptr f, const int size);
extern void mpfrx_swap (mpfrx_ptr f, mpfrx_ptr g);


extern void mpcx_set_deg (mpcx_ptr f, const int deg);
extern void mpcx_set_prec (mpcx_ptr f, const mpfr_prec_t deg);
extern void mpcx_set_coeff (mpcx_ptr f, const unsigned int i, mpc_srcptr coeff);
extern mpfr_ptr mpfrx_get_coeff (mpfrx_srcptr f, const unsigned int i);

extern void mpfrx_set_deg (mpfrx_ptr f, const int deg);
extern void mpfrx_set_prec (mpfrx_ptr f, const mpfr_prec_t deg);
extern void mpfrx_set_coeff (mpfrx_ptr f, const unsigned int i, mpfr_srcptr coeff);
extern mpc_ptr mpcx_get_coeff (mpcx_srcptr f, const unsigned int i);


extern int mpcx_cmp (mpcx_srcptr f, mpcx_srcptr g);
extern void mpcx_urandom (mpcx_ptr f, int deg, gmp_randstate_t state);

extern int mpfrx_cmp (mpfrx_srcptr f, mpfrx_srcptr g);
extern void mpfrx_urandom (mpfrx_ptr f, int deg, gmp_randstate_t state);


extern void mpcx_add (mpcx_ptr h, mpcx_srcptr f, mpcx_srcptr g);
extern void mpcx_sub (mpcx_ptr h, mpcx_srcptr f, mpcx_srcptr g);
extern void mpcx_si_sub (mpcx_ptr h, const long int c, mpcx_srcptr f);
extern void mpcx_neg (mpcx_ptr h, mpcx_srcptr f);
extern void mpcx_mul (mpcx_ptr h, mpcx_srcptr f, mpcx_srcptr g);
extern void mpcx_mul_c (mpcx_ptr h, mpcx_srcptr f, mpc_srcptr g);
extern void mpcx_mul_fr (mpcx_ptr h, mpcx_srcptr f, mpfr_srcptr g);
extern void mpcx_mul_si (mpcx_ptr h, mpcx_srcptr f, const long int g);
extern void mpcx_mul_ui (mpcx_ptr h, mpcx_srcptr f, const unsigned long int g);
extern void mpcx_mul_x (mpcx_ptr h, mpcx_srcptr f, const unsigned int n);

extern void mpfrx_add (mpfrx_ptr h, mpfrx_srcptr f, mpfrx_srcptr g);
extern void mpfrx_sub (mpfrx_ptr h, mpfrx_srcptr f, mpfrx_srcptr g);
extern void mpfrx_si_sub (mpfrx_ptr h, const long int c, mpfrx_srcptr f);
extern void mpfrx_neg (mpfrx_ptr h, mpfrx_srcptr f);
extern void mpfrx_mul (mpfrx_ptr h, mpfrx_srcptr f, mpfrx_srcptr g);
extern void mpfrx_mul_fr (mpfrx_ptr h, mpfrx_srcptr f, mpfr_srcptr g);
extern void mpfrx_mul_si (mpfrx_ptr h, mpfrx_srcptr f, const long int g);
extern void mpfrx_mul_ui (mpfrx_ptr h, mpfrx_srcptr f, const unsigned long int g);
extern void mpfrx_mul_x (mpfrx_ptr h, mpfrx_srcptr f, const unsigned int n);


extern void mpcx_eval (mpc_ptr r, mpcx_srcptr f, mpc_srcptr x);

extern void mpfrx_eval (mpfr_ptr r, mpfrx_srcptr f, mpfr_srcptr x);


extern void mpfrcx_real (mpfrx_ptr h, mpcx_srcptr f);
extern void mpfrcx_imag (mpfrx_ptr h, mpcx_srcptr f);


extern void mpcx_rem (mpcx_ptr r, mpcx_srcptr f, mpcx_srcptr g);

extern void mpfrx_rem (mpfrx_ptr r, mpfrx_srcptr f, mpfrx_srcptr g);


extern void mpcx_derive (mpcx_ptr h, mpcx_srcptr f);
extern void mpcx_root (mpc_ptr root, mpcx_srcptr f);

extern void mpfrx_derive (mpfrx_ptr h, mpfrx_srcptr f);
extern void mpfrx_root (mpfr_ptr root, mpfrx_srcptr f);


extern void mpcx_tree_init (mpcx_tree_ptr t, int no, mpfr_prec_t prec);
extern void mpcx_tree_clear (mpcx_tree_ptr t);
extern void mpcx_subproducttree (mpcx_tree_ptr t, mpcx_t *factors);
extern void mpcx_tree_get_root (mpcx_ptr f, mpcx_tree_srcptr t);
extern void mpcx_reconstruct (mpcx_ptr rop, mpcx_t *factors, int no);
extern void mpcx_multieval (mpc_t *values, mpc_t *args, int no,
   mpcx_srcptr f);
extern void mpcx_hecke (mpcx_ptr rop, mpcx_tree_srcptr subproducts,
   mpcx_t *vals);
extern void mpcx_product_and_hecke (mpcx_t *rop, mpcx_t **vals,
   int no_pols, int no_factors);

extern void mpcx_reconstruct_from_roots (mpcx_ptr rop, mpc_t *roots,
   int no);
extern void mpcx_subproducttree_from_roots (mpcx_tree_ptr t, mpc_t *roots,
   int no);
extern void mpcx_hecke_from_roots (mpcx_ptr rop,
   mpcx_tree_srcptr subproducts, mpc_t *vals);
extern void mpcx_product_and_hecke_from_roots (mpcx_t *rop, mpc_t **vals,
   int no_pols, int no_factors);

extern void mpfrx_tree_init (mpfrx_tree_ptr t, int no, mpfr_prec_t prec);
extern void mpfrx_tree_clear (mpfrx_tree_ptr t);
extern void mpfrx_subproducttree (mpfrx_tree_ptr t, mpfrx_t *factors);
extern void mpfrx_tree_get_root (mpfrx_ptr f, mpfrx_tree_srcptr t);
extern void mpfrx_reconstruct (mpfrx_ptr rop, mpfrx_t *factors, int no);
extern void mpfrx_multieval (mpfr_t *values, mpfr_t *args, int no,
   mpfrx_srcptr f);
extern void mpfrx_hecke (mpfrx_ptr rop, mpfrx_tree_srcptr subproducts,
   mpfrx_t *vals);
extern void mpfrx_product_and_hecke (mpfrx_t *rop, mpfrx_t **vals,
   int no_pols, int no_factors);

extern void mpfrx_reconstruct_from_roots (mpfrx_ptr rop, mpfr_t *roots,
   int no);
extern void mpfrx_subproducttree_from_roots (mpfrx_tree_ptr t, mpfr_t *roots,
   int no);
extern void mpfrx_hecke_from_roots (mpfrx_ptr rop,
   mpfrx_tree_srcptr subproducts, mpfr_t *vals);
extern void mpfrx_product_and_hecke_from_roots (mpfrx_t *rop, mpfr_t **vals,
   int no_pols, int no_factors);

extern void mpfrcx_reconstruct_from_roots (mpfrx_ptr rop, mpc_t *roots,
   int *conjugates, int no);
extern void mpfrcx_subproducttree_from_roots (mpfrx_tree_ptr rop,
   mpc_t *roots, int *conjugates, int no);
extern void mpfrcx_hecke_from_roots (mpfrx_ptr rop,
   mpfrx_tree_srcptr subproducts,
   mpc_t *vals, mpc_t *roots, int *conjugates);
extern void mpfrcx_product_and_hecke_from_roots (mpfrx_t *rop, mpc_t **vals,
   int *conjugates, int no_pols, int no_factors);


extern void mpcx_tower_init (mpcx_tower_ptr twr, int levels, int* d,
   mpfr_prec_t prec);
extern void mpcx_tower_clear (mpcx_tower_ptr twr);
extern void mpcx_tower_decomposition (mpcx_tower_ptr twr, mpc_t *roots);

extern void mpfrx_tower_init (mpfrx_tower_ptr twr, int levels, int* d,
   mpfr_prec_t prec);
extern void mpfrx_tower_clear (mpfrx_tower_ptr twr);
extern void mpfrcx_tower_decomposition (mpfrx_tower_ptr twr, mpc_t *roots,
   int *conjugates);


#ifdef _MPFRCX_H_HAVE_FILE
extern size_t mpcx_out_str (FILE* stream, int base, size_t n_digits, mpcx_srcptr f);

extern size_t mpfrx_out_str (FILE* stream, int base, size_t n_digits, mpfrx_srcptr f);
#endif

#if defined (__cplusplus)
}
#endif

#define mpcx_get_prec(x) (mpc_get_prec((x)->coeff[0]))
#define mpfrx_get_prec(x) (mpfr_get_prec((x)->coeff[0]))

#define mpcx_get_deg(x) ((x)->deg)
#define mpfrx_get_deg(x) ((x)->deg)

#endif /* ifndef __MPFRCX_H */
