/*
mpcx_root

computes a root of f via Newton iterations. Assumes that root contains
an initial approximation to the sought root. Performs no special
tests, so might easily end up in an eternal loop if there is no
convergence.
For the time being, we do not work with increasing, but constant
precision; this could be made more efficient.

Copyright (C) 2009, 2010 Andreas Enge

This file is part of the MPFRCX Library.

The MPFRCX Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The MPFRCX Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MPFRCX library; see the file COPYING.LESSER.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
*/

#include "mpfrcx-impl.h"

void mpcx_root (mpc_ptr root, mpcx_srcptr f) {
   mp_prec_t prec;
   mpc_t value, valuep, power, tmp, tmpp;
   mpfr_t error, max_err;
   int i;
   unsigned int iter;

   prec = mpc_get_prec (root);
   if (f->deg == 0) {
      printf ("*** Calling 'mpcx_root' with a constant polynomial!\n");
      exit (1);
   }
   else if (f->deg == 1) {
      mpc_neg (root, f->coeff [0], MPC_RNDNN);
      mpc_div (root, root, f->coeff [1], MPC_RNDNN);
   }
   else {
      /* add a few bits to the precision. The real number of bits to be   */
      /* added depends on the magnitudes of the derivative; if it is      */
      /* smaller than 1, we lose bits, if it is larger, we gain.          */
      /* For the time being, we just add a constant value.                */
      /* Compute the value itself at a higher precision. Since it is 0,   */
      /* there must be some cancellation.                                 */
      mpc_init2 (value, 2*prec + 10);
      mpc_init2 (valuep, prec + 10);
      mpc_init2 (power, 2*prec + 10);
      mpc_init2 (tmp, 2*prec + 10);
      mpc_init2 (tmpp, prec + 10);
      mpfr_init2 (error, 10);
      mpfr_init2 (max_err, 10);

      /* compute the maximal acceptable absolute error so that the        */
      /* relative error is smaller than 2^{-prec} (relative to the        */
      /* initial approximation of the root)                               */
      mpc_abs (max_err, root, MPC_RNDNN);
      mpfr_div_2exp (max_err, max_err, mpc_get_prec (root), MPC_RNDNN);

      mpfr_set (error, max_err, MPC_RNDNN);
      iter = 0;
      while (mpfr_cmp (error, max_err) >= 0) {
         /* evaluate f and f' in root */
         mpc_set (value, f->coeff [0], MPC_RNDNN);
         mpc_set (valuep, f->coeff [1], MPC_RNDNN);
         mpc_set (power, root, MPC_RNDNN);
         for (i = 1; i < f->deg; i++) {
            mpc_mul (tmp, power, f->coeff [i], MPC_RNDNN);
            mpc_add (value, value, tmp, MPC_RNDNN);
            mpc_mul (tmpp, power, f->coeff [i+1], MPC_RNDNN);
            mpc_mul_ui (tmpp, tmpp, i+1, MPC_RNDNN);
            mpc_add (valuep, valuep, tmpp, MPC_RNDNN);
            mpc_mul (power, power, root, MPC_RNDNN);
         }
         mpc_mul (tmp, power, f->coeff [f->deg], MPC_RNDNN);
         mpc_add (value, value, tmp, MPC_RNDNN);

         /* update root */
         mpc_div (tmp, value, valuep, MPC_RNDNN);
         mpc_sub (root, root, tmp, MPC_RNDNN);
         mpc_abs (error, tmp, MPC_RNDNN);
         iter++;
         if ((mp_prec_t) iter >= prec) {
            printf ("*** Houston, we have a problem! Newton iteration not ");
            printf ("converging.\n");
            exit (1);
         }
      }

      mpc_clear (value);
      mpc_clear (valuep);
      mpc_clear (power);
      mpc_clear (tmp);
      mpc_clear (tmpp);
      mpfr_clear (error);
      mpfr_clear (max_err);
   }
}
