/*
mpcx_eval

computes f (x)

Copyright (C) 2020 Andreas Enge

This file is part of the MPFRCX Library.

The MPFRCX Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The MPFRCX Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MPFRCX library; see the file COPYING.LESSER.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
*/

#include "mpfrcx-impl.h"

void mpcx_eval (mpc_ptr rop, mpcx_srcptr f, mpc_srcptr x) {
   /* Compute f (x) with a Horner scheme. */

   int overlap = rop == x;
   int deg = mpcx_get_deg (f);
   int i;
   mpc_t r;

   if (deg == -1)
      mpc_set_ui (rop, 0, MPC_RNDNN);
   else if (deg == 0)
      /* Could be handled by Horner, but does not pose problems
         with overlap. */
      mpc_set (rop, mpcx_get_coeff (f, 0), MPC_RNDNN);
   else {
      if (overlap)
         mpc_init3 (r, mpfr_get_prec (mpc_realref (rop)),
                       mpfr_get_prec (mpc_imagref (rop)));
      else
         r [0] = rop [0];

      mpc_set (r, mpcx_get_coeff (f, deg), MPC_RNDNN);
      for (i = deg - 1; i >= 0; i--)
         mpc_fma (r, r, x, mpcx_get_coeff (f, i), MPC_RNDNN);

      if (overlap)
         mpc_clear (rop);
      rop [0] = r [0];
   }
}

