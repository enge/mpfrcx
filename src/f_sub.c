/*
mpfrx_sub

computes h = f - g

Copyright (C) 2009 Andreas Enge

This file is part of the MPFRCX Library.

The MPFRCX Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The MPFRCX Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MPFRCX library; see the file COPYING.LESSER.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
*/

#include "mpfrcx-impl.h"

void mpfrx_sub (mpfrx_ptr h, mpfrx_srcptr f, mpfrx_srcptr g) {
   int i;
   int hdeg;
      /* contains h->deg; the latter cannot be set before the end in
         case h==f or h==g */

   hdeg = MAX (f->deg, g->deg);
   /* compute cancellation */
   if (f->deg == g->deg)
      while (hdeg >= 0 && !mpfr_cmp (f->coeff [hdeg], g->coeff [hdeg]))
         hdeg--;

   if (h->size < hdeg + 1)
      mpfrx_realloc (h, hdeg + 1);

   for (i = f->deg; i > g->deg; i--)
      mpfr_set (h->coeff [i], f->coeff [i], GMP_RNDN);
   for (i = g->deg; i > f->deg; i--)
      mpfr_neg (h->coeff [i], g->coeff [i], GMP_RNDN);
   for (i = MIN (MIN (f->deg, g->deg), hdeg); i >= 0; i--)
      mpfr_sub (h->coeff [i], f->coeff [i], g->coeff [i], GMP_RNDN);

   h->deg = hdeg;
}

/*****************************************************************************/

void mpfrx_si_sub (mpfrx_ptr h, const long int f, mpfrx_srcptr g)

{
   int i;

   if (g->deg == -1) {
      if (f == 0)
         h->deg = -1;
      else {
         h->deg = 0;
         if (h->size < 1)
            mpfrx_realloc (h, 1);
         mpfr_set_ui (h->coeff [0], f, GMP_RNDN);
      }
   }
   else if (g->deg == 0 && mpfr_cmp_si (g->coeff [0], f) == 0)
      h->deg = -1;
   else {
      if (h->size < g->deg + 1)
         mpfrx_realloc (h, g->deg + 1);
      h->deg = g->deg;
      for (i = 0; i <= g->deg; i++)
         mpfr_neg (h->coeff [i], g->coeff [i], GMP_RNDN);
      mpfr_add_ui (h->coeff [0], h->coeff [0], f, GMP_RNDN);
   }
}
