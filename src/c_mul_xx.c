/*
mpcx_mul_*

computes h = f * g with g of various types

Copyright (C) 2020 Andreas Enge

This file is part of the MPFRCX Library.

The MPFRCX Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The MPFRCX Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MPFRCX library; see the file COPYING.LESSER.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
*/

#include "mpfrcx-impl.h"

void mpcx_mul_fr (mpcx_ptr h, mpcx_srcptr f, mpfr_srcptr g) {
   int i;

   if (f->deg == -1) {
      h->deg = -1;
      return;
   }

   mpcx_set_deg (h, f->deg);
   for (i = f->deg; i >= 0; i--)
      mpc_mul_fr (h->coeff [i], f->coeff [i], g, MPC_RNDNN);
}


void mpcx_mul_si (mpcx_ptr h, mpcx_srcptr f, const long int g) {
   int i;

   if (f->deg == -1) {
      h->deg = -1;
      return;
   }

   mpcx_set_deg (h, f->deg);
   for (i = f->deg; i >= 0; i--)
      mpc_mul_si (h->coeff [i], f->coeff [i], g, MPC_RNDNN);
}


void mpcx_mul_ui (mpcx_ptr h, mpcx_srcptr f, const unsigned long int g) {
   int i;

   if (f->deg == -1) {
      h->deg = -1;
      return;
   }

   mpcx_set_deg (h, f->deg);
   for (i = f->deg; i >= 0; i--)
      mpc_mul_ui (h->coeff [i], f->coeff [i], g, MPC_RNDNN);
}


void mpcx_mul_x (mpcx_ptr h, mpcx_srcptr f, const unsigned int n) {
   int i;

   if (f->deg == -1) {
      h->deg = -1;
      return;
   }

   mpcx_set_deg (h, f->deg + n);
   for (i = h->deg; i >= (int) n; i--)
      mpc_set (h->coeff [i], f->coeff [i-n], MPC_RNDNN);
   for (i = n-1; i >= 0; i--)
      mpc_set_ui (h->coeff [i], 0, MPC_RNDNN);
}

