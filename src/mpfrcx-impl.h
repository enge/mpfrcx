/*
mpfrcx-impl.h

utility functions for mpfrcx, not exported

Copyright (C) 2009, 2010, 2012, 2018 Andreas Enge

This file is part of the MPFRCX Library.

The MPFRCX Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The MPFRCX Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MPFRCX library; see the file COPYING.LESSER.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
*/

#ifndef __MPFRCX_IMPL_H
#define __MPFRCX_IMPL_H
#include "mpfrcx.h"

#define MPCX_FFT_THRESHOLD 512
#define MPCX_NOFFT_THRESHOLD 1000000
#define MPFRX_FFT_THRESHOLD 512
#define MPFRX_NOFFT_THRESHOLD 1000000

#define MAX(x,y) ((x) > (y) ? (x) : (y))
#define MIN(x,y) ((x) < (y) ? (x) : (y))

#if defined (__cplusplus)
extern "C" {
#endif
extern void mpcx_mv (mpcx_ptr f, mpcx_srcptr g);
extern void mpfrx_mv (mpfrx_ptr f, mpfrx_srcptr g);

extern void mpcx_array_mul_fft (mpc_t *h, mpc_t *f, mpc_t *g, int m, int n);
extern void mpfrx_array_mul_fft (mpfr_t *h, mpfr_t *f, mpfr_t *g,
                                 int m, int n);

extern void mpcx_zeta_init (mpc_t **zeta, int n, mpfr_prec_t prec);
extern void mpcx_zeta_clear (mpc_t *zeta, int n);
extern void mpcx_twiddle_init (mpfr_t **twiddle, int n, mpfr_prec_t prec);
extern void mpcx_twiddle_clear (mpfr_t *twiddle, int n);
extern void mpcx_dftrb (mpc_t *f, int n, mpfr_t *twiddle,
                        int zeta_off, int direction);

extern void mpcx_init_set_linear (mpcx_ptr h, mpc_srcptr z);
extern void mpfrx_init_set_linear (mpfrx_ptr h, mpfr_srcptr z);
extern void mpfrcx_init_set_quadratic (mpfrx_ptr h, mpc_srcptr z);

extern mpfr_prec_t mpfr_coinciding_bits (mpfr_srcptr op1, mpfr_srcptr op2);
extern mpfr_prec_t mpc_coinciding_bits (mpc_srcptr op1, mpc_srcptr op2);
#if defined (__cplusplus)
}
#endif
#endif /* ifndef __MPFRCX_IMPL_H */
