/*
mpfrx_cmp

compares f and g

Copyright (C) 2009 Andreas Enge

This file is part of the MPFRCX Library.

The MPFRCX Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The MPFRCX Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MPFRCX library; see the file COPYING.LESSER.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
*/

#include "mpfrcx-impl.h"

int mpfrx_cmp (mpfrx_srcptr f, mpfrx_srcptr g) {

   if (f->deg != g->deg)
      return -1;
   else {
      int i;
      for (i = f->deg; i >= 0; i--)
         if (mpfr_cmp (f->coeff [i], g->coeff [i]))
            return -1;
      return 0;
   }
}
