/*
mpfrx_realloc
changes the number of coefficients in f to size; preserves the value
of f if it still fits, otherwise replaces it by 0.

Copyright (C) 2009 Andreas Enge

This file is part of the MPFRCX Library.

The MPFRCX Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The MPFRCX Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MPFRCX library; see the file COPYING.LESSER.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
*/

#include "mpfrcx-impl.h"

void mpfrx_realloc (mpfrx_ptr f, const int size) {
   int i;

   if (size > f->size) {
      f->coeff = realloc (f->coeff, size * sizeof (mpfr_t));
      for (i = f->size; i < size; i++)
         mpfr_init2 (f->coeff [i], f->prec);
      f->size = size;
   }
   else if (size < f->size) {
      for (i = size; i < f->size; i++)
         mpfr_clear (f->coeff [i]);
      f->coeff = realloc (f->coeff, size * sizeof (mpfr_t));
      f->size = size;
      if (f->deg >= size)
         f->deg = -1;
   }
}
