/*
mpfrx_derive

computes h = f'

Copyright (C) 2017 Andreas Enge

This file is part of the MPFRCX Library.

The MPFRCX Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The MPFRCX Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MPFRCX library; see the file COPYING.LESSER.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
*/

#include "mpfrcx-impl.h"

void mpfrx_derive (mpfrx_ptr h, mpfrx_srcptr f)
{
   int i;

   if (h->size < f->deg)
      mpfrx_realloc (h, f->deg);

   for (i = 0; i < f->deg; i++)
      mpfr_mul_ui (h->coeff [i], f->coeff [i+1], i+1, GMP_RNDN);
   h->deg = f->deg - 1;
}

