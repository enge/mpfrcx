/*
mpfrx_add

computes h = f + g

Copyright (C) 2009 Andreas Enge

This file is part of the MPFRCX Library.

The MPFRCX Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The MPFRCX Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MPFRCX library; see the file COPYING.LESSER.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
*/

#include "mpfrcx-impl.h"

void mpfrx_add (mpfrx_ptr h, mpfrx_srcptr f, mpfrx_srcptr g) {
   int i, hdeg;

   hdeg = MAX (f->deg, g->deg);
   if (h->size < hdeg + 1)
      mpfrx_realloc (h, hdeg + 1);

   for (i = f->deg; i > g->deg; i--)
      mpfr_set (h->coeff [i], f->coeff [i], GMP_RNDN);
   for (i = g->deg; i > f->deg; i--)
      mpfr_set (h->coeff [i], g->coeff [i], GMP_RNDN);
   for (i = MIN (f->deg, g->deg); i >= 0; i--)
      mpfr_add (h->coeff [i], f->coeff [i], g->coeff [i], GMP_RNDN);

   h->deg = hdeg;
   while (h->deg >= 0 && mpfr_cmp_si (h->coeff [h->deg], 0) == 0)
      h->deg--;
}
