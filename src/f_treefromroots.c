/*
mpfrx_treefromroots

high-level functions working with trees of polynomials, the leaves of
which are linear polynomials obtained from their roots

Copyright (C) 2018 Andreas Enge

This file is part of the MPFRCX Library.

The MPFRCX Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The MPFRCX Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MPFRCX library; see the file COPYING.LESSER.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
*/

#include "mpfrcx-impl.h"

/**************************************************************************/

void mpfrx_init_set_linear (mpfrx_ptr h, mpfr_srcptr z)
   /* Initialise h to the precision of z and set it to X-z. */

{
   mpfr_prec_t prec = mpfr_get_prec (z);

   mpfrx_init (h, 2, prec);
   mpfrx_set_deg (h, 1);
   mpfr_set_ui (h->coeff [1], 1, GMP_RNDN);
   mpfr_neg (h->coeff [0], z, GMP_RNDN);
}

/**************************************************************************/

void mpfrx_reconstruct_from_roots (mpfrx_ptr rop, mpfr_t *roots, int no)
   /* Takes an array of no elements and computes the polynomial having
      these as roots, that is, prod_{i=1}^n (X - roots[i]); the real
      and imaginary part of each element of roots must have the same
      precision. */

{
   int i;
   mpfrx_t *fac;

   fac = (mpfrx_t *) malloc (no * sizeof (mpfrx_t));
   for (i = 0; i < no; i++)
      mpfrx_init_set_linear (fac [i], roots [i]);

   mpfrx_reconstruct (rop, fac, no);

   for (i = 0; i < no; i++)
      mpfrx_clear (fac [i]);
   free (fac);
}

/**************************************************************************/

void mpfrx_subproducttree_from_roots (mpfrx_tree_ptr rop, mpfr_t *roots,
   int no)
   /* Takes an array of no elements and initialises and computes the
      subproduct tree having the linear polynomials X - roots [i]
      as leaves. All roots are assumed to be of the same precision,
      which is used for the initialisation. */

{
   int i;
   mpfrx_t *fac;
   mpfr_prec_t prec = mpfr_get_prec (roots [0]);

   mpfrx_tree_init (rop, no, prec);
   fac = (mpfrx_t *) malloc (no * sizeof (mpfrx_t));
   for (i = 0; i < no; i++)
      mpfrx_init_set_linear (fac [i], roots [i]);

   mpfrx_subproducttree (rop, fac);

   for (i = 0; i < no; i++)
      mpfrx_clear (fac [i]);
   free (fac);
}

/**************************************************************************/

void mpfrx_hecke_from_roots (mpfrx_ptr rop, mpfrx_tree_srcptr subproducts,
   mpfr_t *vals)
   /* The function works exactly as mpfrx_hecke, but assumes that the
      subproduct tree has been generated (for instance by a call to
      mpfrx_subproducttree_from_roots) from monic linear polynomials;
      then vals is an array of constant polynomials, which may simply
      be passed as complex numbers. */

{
   int i, n = subproducts->no;
   mpfr_prec_t prec = mpfr_get_prec (vals [0]);
   mpfrx_t *fac;

   fac = (mpfrx_t *) malloc (n * sizeof (mpfrx_t));
   for (i = 0; i < n; i++) {
      mpfrx_init (fac [i], 1, prec);
      mpfrx_set_deg (fac [i], 0);
      mpfrx_set_coeff (fac [i], 0, vals [i]);
   }

   mpfrx_hecke (rop, subproducts, fac);

   for (i = 0; i < n; i++)
      mpfrx_clear (fac [i]);
   free (fac);
}

/**************************************************************************/

void mpfrx_product_and_hecke_from_roots (mpfrx_t *rop, mpfr_t **vals,
   int no_pols, int no_factors)
   /* The entries of val are expected to have the same precision. */

{
   int i, j;
   mpfr_prec_t prec = mpfr_get_prec (vals [0][0]);
   mpfrx_t **valx;

   valx = (mpfrx_t **) malloc (no_pols * sizeof (mpfrx_t *));
   for (i = 0; i < no_pols; i++)
      valx [i] = (mpfrx_t *) malloc (no_factors * sizeof (mpfrx_t));
   for (j = 0; j < no_factors; j++)
      mpfrx_init_set_linear (valx [0][j], vals [0][j]);
   for (i = 1; i < no_pols; i++)
      for (j = 0; j < no_factors; j++) {
         mpfrx_init (valx [i][j], 1, prec);
         mpfrx_set_deg (valx [i][j], 0);
         mpfrx_set_coeff (valx [i][j], 0, vals [i][j]);
      }

   mpfrx_product_and_hecke (rop, valx, no_pols, no_factors);

   for (i = 0; i < no_pols; i++) {
      for (j = 0; j < no_factors; j++)
         mpfrx_clear (valx [i][j]);
      free (valx [i]);
   }
   free (valx);
}

/**************************************************************************/
