/*
mpcx_sub

computes h = f - g

Copyright (C) 2009 Andreas Enge

This file is part of the MPFRCX Library.

The MPFRCX Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The MPFRCX Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MPFRCX library; see the file COPYING.LESSER.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
*/

#include "mpfrcx-impl.h"

void mpcx_sub (mpcx_ptr h, mpcx_srcptr f, mpcx_srcptr g) {
   int i;
   int hdeg;
      /* contains h->deg; the latter cannot be set before the end in
         case h==f or h==g */

   hdeg = MAX (f->deg, g->deg);
   /* compute cancellation */
   if (f->deg == g->deg)
      while (hdeg >= 0 && !mpc_cmp (f->coeff [hdeg], g->coeff [hdeg]))
         hdeg--;

   if (h->size < hdeg + 1)
      mpcx_realloc (h, hdeg + 1);

   for (i = f->deg; i > g->deg; i--)
      mpc_set (h->coeff [i], f->coeff [i], MPC_RNDNN);
   for (i = g->deg; i > f->deg; i--)
      mpc_neg (h->coeff [i], g->coeff [i], MPC_RNDNN);
   for (i = MIN (MIN (f->deg, g->deg), hdeg); i >= 0; i--)
      mpc_sub (h->coeff [i], f->coeff [i], g->coeff [i], MPC_RNDNN);

   h->deg = hdeg;
}

/*****************************************************************************/

void mpcx_si_sub (mpcx_ptr h, const long int f, mpcx_srcptr g)

{
   int i;

   if (g->deg == -1) {
      if (f == 0)
         h->deg = -1;
      else {
         h->deg = 0;
         if (h->size < 1)
            mpcx_realloc (h, 1);
         mpc_set_ui (h->coeff [0], f, MPC_RNDNN);
      }
   }
   else if (g->deg == 0 && mpc_cmp_si (g->coeff [0], f) == 0)
      h->deg = -1;
   else {
      if (h->size < g->deg + 1)
         mpcx_realloc (h, g->deg + 1);
      h->deg = g->deg;
      for (i = 0; i <= g->deg; i++)
         mpc_neg (h->coeff [i], g->coeff [i], MPC_RNDNN);
      mpc_add_ui (h->coeff [0], h->coeff [0], f, MPC_RNDNN);
   }
}
