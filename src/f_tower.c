/*
mpfrx_tower

functions working with towers of the real subfields of abelian extensions
of \Q (\sqrt D), where D is a negative discriminant

Copyright (C) 2018, 2021 Andreas Enge

This file is part of the MPFRCX Library.

The MPFRCX Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The MPFRCX Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MPFRCX library; see the file COPYING.LESSER.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
*/

#include "mpfrcx-impl.h"

static int mpfrcx_is_galois_generator (mpc_t *embeddings, int *conjugates,
   int n);
static void mpfrcx_tower_decomposition_rec (int levels, int *d,
   mpfrx_t **W, mpfrx_ptr V, mpc_t *roots, int *conjugates);

/**************************************************************************/

static int mpfrcx_is_galois_generator (mpc_t *embeddings, int *conjugates,
   int n)
   /* Given the n complex embeddings of a real algebraic number in a Galois
      number field, the function returns 1 if the number is (modulo rounding
      errors) a generator of the field and 0 otherwise. The meaning of
      conjugates is as usual to keep track of real and pairs of complex
      roots.
      The implementation is copied over from mpcx_is_galois_generator
      in c_tower.c; but we must make sure to consider only roots that
      have been set following conjugates.
      Additionally, it may happen that two embeddings marked as a complex
      conjugate pair have in fact become the same real number; we consider
      that this is the case when the imaginary part is sufficiently small
      compared to the real part. */
{
   int i;
   mpfr_prec_t prec, min_prec;

   prec = mpc_get_prec (embeddings [0]);
   min_prec = prec / 2;

   for (i = 1; i < n; i++)
      if (conjugates [i] >= i &&
         mpc_coinciding_bits (embeddings [0], embeddings [i]) > min_prec)
         return 0;

   for (i = 0; i < n; i++)
      if (conjugates [i] > i &&
         mpfr_get_exp (mpc_realref (embeddings [i]))
            - mpfr_get_exp (mpc_imagref (embeddings [i])) > min_prec)
         return 0;

   return 1;
}

/**************************************************************************/

void mpfrx_tower_init (mpfrx_tower_ptr twr, int levels, int* d,
   mpfr_prec_t prec)
   /* Initialise the tower field structure twr with levels field
      extensions, the relative degrees of which are passed via d.
      The common precision is given by prec. */

{
   int i, j, deg;

   twr->levels = levels;
   twr->d = (int *) malloc (levels * sizeof (int));
   deg = 1;
   for (i = 0; i < levels; i++) {
      twr->d [i] = d [i];
      deg *= d [i];
   }
   twr->deg = deg;
   twr->W = (mpfrx_t **) malloc (levels * sizeof (mpfrx_t *));
   deg = 1;
   for (i = 1; i < levels; i++) {
      twr->W [i] = (mpfrx_t *) malloc ((d [i] + 1) * sizeof (mpfrx_t));
      deg *= d [i - 1]; /* absolute degree of coefficients of W [i] */
      for (j = 0; j <= d [i]; j++)
         mpfrx_init (twr->W [i][j], deg, prec);
   }
   twr->W [0] = (mpfrx_t *) malloc (1 * sizeof (mpfrx_t));
   mpfrx_init (twr->W [0][0], d [0] + 1, prec);
}

/**************************************************************************/

void mpfrx_tower_clear (mpfrx_tower_ptr twr)
   /* Free all dynamically allocated components of a field tower
      structure. */

{
   int i, j;

   for (i = 1; i < twr->levels; i++) {
      for (j = 0; j <= twr->d [i]; j++)
         mpfrx_clear (twr->W [i][j]);
      free (twr->W [i]);
   }
   mpfrx_clear (twr->W [0][0]);
   free (twr->W [0]);
   free (twr->W);
   free (twr->d);
}

/**************************************************************************/

static void mpfrcx_tower_decomposition_rec (int levels, int *d,
   mpfrx_t **W, mpfrx_ptr V, mpc_t *roots, int *conjugates)
   /* This function works as mpcx_tower_decomposition_rec, but its intended
      use is for the real subfields of Hilbert or ray class fields of
      imaginary-quadratic fields. These are extensions of Q with at least
      one real embedding. For instance, the roots of the polynomial defining
      the extension and passed through the variable roots are either real
      or come in complex conjugate pairs. The array conjugates conveys this
      information: conjugates [i] == j means that roots [j] is the complex
      conjugate of roots [i]. In particular, i == j means that the root
      is real. Only one out of a pair of complex conjugates needs to be
      filled in, namely the first one, with i < j. This property is preserved
      throughout the computations, since it is compatible with the splitting
      of roots into orbits: Via conjugates, orbits are in bijection with
      each other; orbits in bijection with themselves lead to real
      elementary symmetric functions as generators of the subfields, the
      other orbits lead to elementary symmetric generators that are complex
      conjugates of each other.
      This function modifies roots and conjugates in preparation for
      recursive calls. */

{
   int i, j, m, n, found;
   mpfr_prec_t prec;
   mpcx_t *U;
   int *local_conj;
   mpfrx_t Ur;
   mpc_t **vals;

   /* Set the precision and the degrees. */
   prec = mpfrx_get_prec (V);
   m = d [levels-1];
   n = 1;
   for (i = 0; i < levels-1; i++)
      n *= d [i];

   /* Compute as floating point polynomials the n relative minimal
      polynomials U [k] of degree m of the conjugates of x over L;
      these polynomials need not all be real, but at least one of them is,
      and they come in complex conjugate pairs. Compute only one out of
      each pair. */
   U = (mpcx_t *) malloc (n * sizeof (mpcx_t));
   mpfrx_init (Ur, m+1, prec); /* temporary variable for the real U [i] */
   local_conj = (int *) malloc (m * sizeof (int));
   for (i = 0; i < n; i++) {
      /* The polynomial is real when the complex conjugate of the first root
         is in the same group of m roots. */
      if (conjugates [m*i] >= m*i && conjugates [m*i] < m*(i+1)) {
         mpcx_init (U [i], m+1, prec);
         for (j = 0; j < m; j++)
            local_conj [j] = conjugates [m*i+j] - m*i;
         mpfrcx_reconstruct_from_roots (Ur, roots + m*i, local_conj, m);
         mpcx_set_frx (U [i], Ur);
      }
      else if (conjugates [m*i] > m*i) {
         mpcx_init (U [i], m+1, prec);
         mpcx_reconstruct_from_roots (U [i], roots + m*i, m);
      }
   }
   mpfrx_clear (Ur);
   free (local_conj);

   /* y is one of the coefficients of U [0], and its conjugates are the
      corresponding coefficients of all the U [j]. Find a generator of
      the intermediate field among them, starting with the trace, which
      is the smallest candidate.
      To prepare the recursion, we replace the content of the array root
      by the conjugates of y, and also keep track of complex conjugates. */
   for (i = 0; i < n; i++)
      conjugates [i] = conjugates [m*i] / m;
   found = 0;
   j = m-1;
   while (!found && j >= 0) {
      for (i = 0; i < n; i++)
         if (conjugates [i] >= i)
            mpc_set (roots [i], mpcx_get_coeff (U [i], j), MPC_RNDNN);
      if (mpfrcx_is_galois_generator (roots, conjugates, n))
         found = 1;
      else
         j--;
   }
   if (!found) {
      printf ("*** Houston, we have a problem!\n");
      printf ("*** No generator found in mpcx_tower_compute_rec.\n");
      printf ("*** Go back programming!\n");
      exit (1);
   }

   /* Compute V, the minimal polynomial of y, in a subproduct tree and
      simultaneously the W [levels-1], containing the Hecke representations
      of the coefficients of U [0]. Use W [levels-1] as a temporary
      variable and adjust later, since there is a shift by 1 in the result
      of mpfrcx_product_and_hecke_from_roots compared to the layout of our
      variables. */
   vals = (mpc_t **) malloc ((m+1) * sizeof (mpc_t *));
   for (j = 0; j <= m; j++)
      vals [j] = (mpc_t *) malloc (n * sizeof (mpc_t));
   for (i = 0; i < n; i++)
      if (conjugates [i] >= i) {
         mpc_init2 (vals [0][i], prec);
         mpc_set (vals [0][i], roots [i], MPC_RNDNN);
         for (j = 0; j < m; j++) {
            mpc_init2 (vals [j+1][i], prec);
            mpc_set (vals [j+1][i], mpcx_get_coeff (U [i], j), MPC_RNDNN);
         }
         mpcx_clear (U [i]);
      }
   free (U);

   mpfrcx_product_and_hecke_from_roots (W [levels-1], vals, conjugates,
      m+1, n);

   /* Shift the results back by 1 and replace W [levels-1][m] by the
      derivative of V. */
   mpfrx_set (V, W [levels-1][0]);
   for (j = 0; j < m; j++)
      mpfrx_set (W [levels-1][j], W [levels-1][j+1]);
   mpfrx_derive (W [levels-1][m], V);

   for (j = 0; j <= m; j++) {
      for (i = 0; i < n; i++)
         if (conjugates [i] >= i)
            mpc_clear (vals [j][i]);
      free (vals [j]);
   }
   free (vals);

   /* Recurse. */
   if (levels >= 3)
      mpfrcx_tower_decomposition_rec (levels - 1, d, W, V, roots,
         conjugates);
}

/**************************************************************************/

void mpfrcx_tower_decomposition (mpfrx_tower_ptr twr, mpc_t *roots,
   int *conjugates)
   /* The function works as mpcx_tower_decomposition, with the additional
      properties and requirements as explained in
      mpfrx_tower_decomposition_rec. */

{
   mpfr_prec_t prec;
   mpc_t *r;
   int *c;
   int i;

   if (twr->levels == 1)
      mpfrcx_reconstruct_from_roots (twr->W [0][0], roots, conjugates,
         twr->d [0]);
   else {
      /* Copy roots and conjugates, which are destroyed by the recursive
         function, and make sure that all entries of the roots array are
         initialised, since they may be accessed during recursion. */
      prec = mpfrx_get_prec (twr->W [0][0]);
      r = (mpc_t *) malloc (twr->deg * sizeof (mpc_t));
      c = (int *) malloc (twr->deg * sizeof (int));
      for (i = 0; i < twr->deg; i++) {
         c [i] = conjugates [i];
         mpc_init2 (r [i], prec);
         if (c [i] >= i)
            mpc_set (r [i], roots [i], MPC_RNDNN);
      }
      mpfrcx_tower_decomposition_rec (twr->levels, twr->d,
         twr->W, twr->W [0][0], r, c);
      for (i = 0; i < twr->deg; i++)
         mpc_clear (r [i]);
      free (r);
      free (c);
   }
}

/**************************************************************************/
