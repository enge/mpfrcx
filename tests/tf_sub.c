/*
test file for sub

Copyright (C) 2009, 2010 Andreas Enge

This file is part of the MPFRCX Library.

The MPFRCX Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The MPFRCX Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MPFRCX library; see the file COPYING.LESSER.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
*/

#include <stdio.h>
#include "mpfrcx.h"


static void error_zero (mpfrx_t h, mpfrx_t f) {
   fprintf (stderr, "Error in sub: f - f yields h with\nf: ");
   mpfrx_out_str (stderr, 10, 0, f);
   fprintf (stderr, "\nh: ");
   mpfrx_out_str (stderr, 10, 0, h);
   fprintf (stderr, "\n");
   exit (1);
}


static void error (mpfrx_t h1, mpfrx_t h2, mpfrx_t f, mpfrx_t g) {
   fprintf (stderr, "Error in sub: f - g yields h1 and h2 with\nf: ");
   mpfrx_out_str (stderr, 10, 0, f);
   fprintf (stderr, "\ng: ");
   mpfrx_out_str (stderr, 10, 0, g);
   fprintf (stderr, "\nh1: ");
   mpfrx_out_str (stderr, 10, 0, h1);
   fprintf (stderr, "\nh2: ");
   mpfrx_out_str (stderr, 10, 0, h2);
   fprintf (stderr, "\n");
   exit (1);
}


static void error_si (mpfrx_t h1, mpfrx_t h2, const long int f, mpfrx_t g) {
   fprintf (stderr,
      "Error in si_sub: f - g yields h1 and h2 with\nf: %li\ng", f);
   mpfrx_out_str (stderr, 10, 0, g);
   fprintf (stderr, "\nh1: ");
   mpfrx_out_str (stderr, 10, 0, h1);
   fprintf (stderr, "\nh2: ");
   mpfrx_out_str (stderr, 10, 0, h2);
   fprintf (stderr, "\n");
   exit (1);
}


static void check_sub (mpfrx_t f, mpfrx_t g) {
   /* checks whether subtraction in place works; f and g must have the
      same precision */
   mpfrx_t f2, g2, h;

   mpfrx_init_set (f2, f);
   mpfrx_init_set (g2, g);
   mpfrx_init (h, 10, mpfrx_get_prec (f));

   mpfrx_sub (h, f, g);
   /* check for 0 if f == g */
   if (!mpfrx_cmp (f, g) && h->deg != -1)
       error_zero (h, f);

   mpfrx_sub (f2, f2, g);
   /* check for difference between normal and in place operation */
   if (mpfrx_cmp (h, f2))
      error (h, f2, f, g);

   mpfrx_sub (g2, f, g2);
   /* check for difference between normal and in place operation */
   if (mpfrx_cmp (h, g2))
      error (h, g2, f, g);

   mpfrx_clear (f2);
   mpfrx_clear (g2);
   mpfrx_clear (h);
}


static void check_si_sub (const long int f, mpfrx_t g) {
   mpfrx_t g2, h;

   mpfrx_init_set (g2, g);
   mpfrx_init (h, 10, mpfrx_get_prec (g));

   mpfrx_si_sub (h, f, g);

   mpfrx_si_sub (g2, f, g2);
   /* check for difference between normal and in place operation */
   if (mpfrx_cmp (h, g2))
      error_si (h, g2, f, g);

   mpfrx_clear (g2);
   mpfrx_clear (h);
}


static void check_sub_random (gmp_randstate_t state) {
   int deg;
   long int f0;
   mpfrx_t f, g;

   mpfrx_init (f, 10, 103);
   mpfrx_init (g, 11, 103);

   for (deg = -1; deg <= 1000; deg += 10) {
      mpfrx_urandom (f, deg, state);
      mpfrx_urandom (g, deg, state);
      check_sub (f, g);
      check_sub (f, f);
      f0 = (long int) gmp_urandomb_ui (state, sizeof (long int) / 8);
      check_si_sub (f0, g);
   }

   mpfrx_clear (f);
   mpfrx_clear (g);
}


int main (void) {
   gmp_randstate_t state;

   gmp_randinit_default (state);

   check_sub_random (state);

   gmp_randclear (state);

   return 0;
}
