/*
test file for neg

Copyright (C) 2009, 2010 Andreas Enge

This file is part of the MPFRCX Library.

The MPFRCX Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The MPFRCX Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MPFRCX library; see the file COPYING.LESSER.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
*/

#include <stdio.h>
#include "mpfrcx.h"


static void error (mpfrx_t h, mpfrx_t f) {
   fprintf (stderr, "Error in neg: -(-f) yields h with\nf: ");
   mpfrx_out_str (stderr, 16, 0, f);
   fprintf (stderr, "\nh: ");
   mpfrx_out_str (stderr, 16, 0, h);
   fprintf (stderr, "\n");
   exit (1);
}


static void check_neg (mpfrx_t f) {
   mpfrx_t h;

   mpfrx_init (h, 10, mpfrx_get_prec (f));

   mpfrx_neg (h, f);
   mpfrx_neg (h, h);
   if (mpfrx_cmp (h, f))
      error (h, f);

   mpfrx_clear (h);
}


static void check_neg_random (gmp_randstate_t state) {
   int deg;
   mpfrx_t f;

   mpfrx_init (f, 10, 103);

   for (deg = -1; deg <= 100; deg++) {
      mpfrx_urandom (f, deg, state);
      check_neg (f);
   }

   mpfrx_clear (f);
}


int main (void) {
   gmp_randstate_t state;

   gmp_randinit_default (state);

   check_neg_random (state);

   gmp_randclear (state);

   return 0;
}
