/*
test file for tree functions

Copyright (C) 2011, 2012 Andreas Enge

This file is part of the MPFRCX Library.

The MPFRCX Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The MPFRCX Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MPFRCX library; see the file COPYING.LESSER.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
*/

#include <stdio.h>
#include "mpfrcx.h"


static void check_reconstruct_multieval () {

   const int n=12;
   const mpfr_prec_t prec = 100;
   int i;
   mpfr_t *values, *args;
   mpfrx_t f;

   values = (mpfr_t *) malloc (n * sizeof (mpfr_t));
   args = (mpfr_t *) malloc (n * sizeof (mpfr_t));
   for (i = 0; i < n; i++) {
      mpfr_init2 (values [i], prec);
      mpfr_init2 (args [i], prec);
      mpfr_set_ui (args [i], (unsigned long int) i, GMP_RNDN);
   }
   
   mpfrx_init (f, n+1, prec);
   mpfrx_reconstruct_from_roots (f, args, n);
   mpfrx_multieval (values, args, n, f);
   mpfrx_clear (f);

   for (i = 0; i < n; i++) {
      if (mpfr_cmp (values [i], args [0]) != 0) {
         fprintf (stderr, "Error in reconstruct or multieval: Value should be 0, but is\n");
         mpfr_out_str (stdout, 10, 0, values [i], GMP_RNDN);
         fprintf (stderr, "\n");
         exit (1);
      }
      mpfr_clear (values [i]);
      mpfr_clear (args [i]);
   }
   free (values);
   free (args);
}


static void
check_hecke (void) {
   const mpfr_prec_t prec = 100;
   int i, j;
   mpfrx_t factor [3], f;
   mpfrx_tree_t subprods;
   mpfrx_t val [3], res;
   mpfrx_t **vals, *ress;

   mpfrx_init (f, 5, prec);
   mpfrx_init (res, 4, prec);
   for (j = 0; j < 3; j++) {
      mpfrx_init (factor [j], 3, prec);
      mpfrx_init (val [j], 2, prec);
   }
   ress = (mpfrx_t *) malloc (3 * sizeof (mpfrx_t));
   vals = (mpfrx_t **) malloc (3 * sizeof (mpfrx_t *));
   for (i = 0; i < 3; i++) {
      mpfrx_init (ress [i], 5, prec);
      vals [i] = (mpfrx_t *) malloc (3 * sizeof (mpfrx_t));
      for (j = 0; j < 3; j++)
         mpfrx_init (vals [i][j], 3, prec);
   }
   mpfrx_tree_init (subprods, 3, prec);

   mpfrx_set_deg (factor [0], 2);
   mpfr_set_ui (factor [0]->coeff [2], 1ul, GMP_RNDN);
   mpfr_set_ui (factor [0]->coeff [1], 0ul, GMP_RNDN);
   mpfr_set_ui (factor [0]->coeff [0], 1ul, GMP_RNDN);
   mpfrx_set_deg (factor [1], 1);
   mpfr_set_ui (factor [1]->coeff [1], 1ul, GMP_RNDN);
   mpfr_set_si (factor [1]->coeff [0], -1l, GMP_RNDN);
   mpfrx_set_deg (factor [2], 1);
   mpfr_set_ui (factor [2]->coeff [1], 1ul, GMP_RNDN);
   mpfr_set_si (factor [2]->coeff [0], -2l, GMP_RNDN);
   mpfrx_set_deg (val [0], 1);
   mpfr_set_ui (val [0]->coeff [1], 2ul, GMP_RNDN);
   mpfr_set_si (val [0]->coeff [0], -2l, GMP_RNDN);
   mpfrx_set_deg (val [1], 0);
   mpfr_set_ui (val [1]->coeff [0], 3ul, GMP_RNDN);
   mpfrx_set_deg (val [2], 0);
   mpfr_set_ui (val [2]->coeff [0], 4ul, GMP_RNDN);
   mpfrx_set_deg (res, 3);
   mpfr_set_ui (res->coeff [3], 9ul, GMP_RNDN);
   mpfr_set_si (res->coeff [2], -18l, GMP_RNDN);
   mpfr_set_ui (res->coeff [1], 17ul, GMP_RNDN);
   mpfr_set_si (res->coeff [0], -14l, GMP_RNDN);

   mpfrx_subproducttree (subprods, factor);
   mpfrx_hecke (f, subprods, val);

   if (mpfrx_cmp (f, res) != 0) {
      fprintf (stderr, "Error in hecke\n");
      printf ("f   "); mpfrx_out_str (stdout, 10, 0, f); printf ("\n");
      printf ("res "); mpfrx_out_str (stdout, 10, 0, res); printf ("\n");
      exit (1);
   }

   for (j = 0; j < 3; j++)
      mpfrx_set (vals [0][j], factor [j]);
   for (i = 1; i < 3; i++)
      for (j = 0; j < 3; j++)
         mpfrx_set (vals [i][j], val [j]);

   mpfrx_product_and_hecke (ress, vals, 3, 3);
   mpfrx_tree_get_root (f, subprods);
   if (mpfrx_cmp (f, ress [0]) != 0) {
      fprintf (stderr, "Error in product_and_hecke\n");
      printf ("f        "); mpfrx_out_str (stdout, 10, 0, f); printf ("\n");
      printf ("ress [0] "); mpfrx_out_str (stdout, 10, 0, ress [0]); printf ("\n");
      exit (1);
   }
   if (mpfrx_cmp (res, ress [1]) != 0) {
      fprintf (stderr, "Error in product_and_hecke\n");
      printf ("res      "); mpfrx_out_str (stdout, 10, 0, res); printf ("\n");
      printf ("ress [1] "); mpfrx_out_str (stdout, 10, 0, ress [1]); printf ("\n");
      exit (1);
   }
   if (mpfrx_cmp (res, ress [2]) != 0) {
      fprintf (stderr, "Error in product_and_hecke\n");
      printf ("res      "); mpfrx_out_str (stdout, 10, 0, res); printf ("\n");
      printf ("ress [2] "); mpfrx_out_str (stdout, 10, 0, ress [2]); printf ("\n");
      exit (1);
   }

   mpfrx_clear (f);
   mpfrx_clear (res);
   for (j = 0; j < 3; j++) {
      mpfrx_clear (factor [j]);
      mpfrx_clear (val [j]);
   }
   for (i = 0; i < 3; i++) {
      mpfrx_clear (ress [i]);
      for (j = 0; j < 3; j++)
         mpfrx_clear (vals [i][j]);
      free (vals [i]);
   }
   free (ress);
   free (vals);
   mpfrx_tree_clear (subprods);
}


int main (void) {

   check_reconstruct_multieval ();
   check_hecke ();

   return 0;
}
