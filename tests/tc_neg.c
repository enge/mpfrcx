/*
test file for neg

Copyright (C) 2009, 2010 Andreas Enge

This file is part of the MPFRCX Library.

The MPFRCX Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The MPFRCX Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MPFRCX library; see the file COPYING.LESSER.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
*/

#include <stdio.h>
#include "mpfrcx.h"


static void error (mpcx_t h, mpcx_t f) {
   fprintf (stderr, "Error in neg: -(-f) yields h with\nf: ");
   mpcx_out_str (stderr, 16, 0, f);
   fprintf (stderr, "\nh: ");
   mpcx_out_str (stderr, 16, 0, h);
   fprintf (stderr, "\n");
   exit (1);
}


static void check_neg (mpcx_t f) {
   mpcx_t h;

   mpcx_init (h, 10, mpcx_get_prec (f));

   mpcx_neg (h, f);
   mpcx_neg (h, h);
   if (mpcx_cmp (h, f))
      error (h, f);

   mpcx_clear (h);
}


static void check_neg_random (gmp_randstate_t state) {
   int deg;
   mpcx_t f;

   mpcx_init (f, 10, 103);

   for (deg = -1; deg <= 100; deg++) {
      mpcx_urandom (f, deg, state);
      check_neg (f);
   }

   mpcx_clear (f);
}


int main (void) {
   gmp_randstate_t state;

   gmp_randinit_default (state);

   check_neg_random (state);

   gmp_randclear (state);

   return 0;
}
