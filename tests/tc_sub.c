/*
test file for sub

Copyright (C) 2009, 2010 Andreas Enge

This file is part of the MPFRCX Library.

The MPFRCX Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The MPFRCX Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MPFRCX library; see the file COPYING.LESSER.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
*/

#include <stdio.h>
#include "mpfrcx.h"


static void error_zero (mpcx_t h, mpcx_t f) {
   fprintf (stderr, "Error in sub: f - f yields h with\nf: ");
   mpcx_out_str (stderr, 10, 0, f);
   fprintf (stderr, "\nh: ");
   mpcx_out_str (stderr, 10, 0, h);
   fprintf (stderr, "\n");
   exit (1);
}


static void error (mpcx_t h1, mpcx_t h2, mpcx_t f, mpcx_t g) {
   fprintf (stderr, "Error in sub: f - g yields h1 and h2 with\nf: ");
   mpcx_out_str (stderr, 10, 0, f);
   fprintf (stderr, "\ng: ");
   mpcx_out_str (stderr, 10, 0, g);
   fprintf (stderr, "\nh1: ");
   mpcx_out_str (stderr, 10, 0, h1);
   fprintf (stderr, "\nh2: ");
   mpcx_out_str (stderr, 10, 0, h2);
   fprintf (stderr, "\n");
   exit (1);
}


static void error_si (mpcx_t h1, mpcx_t h2, const long int f, mpcx_t g) {
   fprintf (stderr,
      "Error in si_sub: f - g yields h1 and h2 with\nf: %li\ng", f);
   mpcx_out_str (stderr, 10, 0, g);
   fprintf (stderr, "\nh1: ");
   mpcx_out_str (stderr, 10, 0, h1);
   fprintf (stderr, "\nh2: ");
   mpcx_out_str (stderr, 10, 0, h2);
   fprintf (stderr, "\n");
   exit (1);
}


static void check_sub (mpcx_t f, mpcx_t g) {
   /* checks whether subtraction in place works; f and g must have the
      same precision */
   mpcx_t f2, g2, h;

   mpcx_init_set (f2, f);
   mpcx_init_set (g2, g);
   mpcx_init (h, 10, mpcx_get_prec (f));

   mpcx_sub (h, f, g);
   /* check for 0 if f == g */
   if (!mpcx_cmp (f, g) && h->deg != -1)
       error_zero (h, f);

   mpcx_sub (f2, f2, g);
   /* check for difference between normal and in place operation */
   if (mpcx_cmp (h, f2))
      error (h, f2, f, g);

   mpcx_sub (g2, f, g2);
   /* check for difference between normal and in place operation */
   if (mpcx_cmp (h, g2))
      error (h, g2, f, g);

   mpcx_clear (f2);
   mpcx_clear (g2);
   mpcx_clear (h);
}


static void check_si_sub (const long int f, mpcx_t g) {
   mpcx_t g2, h;

   mpcx_init_set (g2, g);
   mpcx_init (h, 10, mpcx_get_prec (g));

   mpcx_si_sub (h, f, g);

   mpcx_si_sub (g2, f, g2);
   /* check for difference between normal and in place operation */
   if (mpcx_cmp (h, g2))
      error_si (h, g2, f, g);

   mpcx_clear (g2);
   mpcx_clear (h);
}


static void check_sub_random (gmp_randstate_t state) {
   int deg;
   long int f0;
   mpcx_t f, g;

   mpcx_init (f, 10, 103);
   mpcx_init (g, 11, 103);

   for (deg = -1; deg <= 1000; deg += 10) {
      mpcx_urandom (f, deg, state);
      mpcx_urandom (g, deg, state);
      check_sub (f, g);
      check_sub (f, f);
      f0 = (long int) gmp_urandomb_ui (state, sizeof (long int) / 8);
      check_si_sub (f0, g);
   }

   mpcx_clear (f);
   mpcx_clear (g);
}


int main (void) {
   gmp_randstate_t state;

   gmp_randinit_default (state);

   check_sub_random (state);

   gmp_randclear (state);

   return 0;
}
