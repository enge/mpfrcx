/*
test file for swap

Copyright (C) 2012 Andreas Enge

This file is part of the MPFRCX Library.

The MPFRCX Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The MPFRCX Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MPFRCX library; see the file COPYING.LESSER.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
*/

#include <stdio.h>
#include "mpfrcx.h"


int main (void) {
   gmp_randstate_t state;
   mpfrx_t f, g, fc, gc;
   int maxdeg = 100;
   mpfr_prec_t fprec = 213, gprec = 117;

   gmp_randinit_default (state);
   mpfrx_init (f, maxdeg + 1, fprec);
   mpfrx_init (g, maxdeg + 1, gprec);
   mpfrx_urandom (f, maxdeg, state);
   mpfrx_urandom (g, maxdeg, state);
   mpfrx_init_set (fc, f);
   mpfrx_init_set (gc, g);

   mpfrx_swap (f, g);
   if (mpfrx_get_prec (f) != gprec || mpfrx_get_prec (g) != fprec) {
      fprintf (stderr, "Error in swap: precisions not swapped correctly\n");
      exit (1);
   }
   if (mpfrx_cmp (f, gc) != 0 || mpfrx_cmp (g, fc) != 0) {
      fprintf (stderr, "Error in swap: content of variable not swapped correctly\n");
      exit (1);
   }

   gmp_randclear (state);
   mpfrx_clear (f);
   mpfrx_clear (g);
   mpfrx_clear (fc);
   mpfrx_clear (gc);

   return 0;
}
