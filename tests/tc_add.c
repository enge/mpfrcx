/*
test file for add

Copyright (C) 2009, 2010 Andreas Enge

This file is part of the MPFRCX Library.

The MPFRCX Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The MPFRCX Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MPFRCX library; see the file COPYING.LESSER.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
*/

#include <stdio.h>
#include "mpfrcx.h"


static void error (mpcx_t h1, mpcx_t h2, mpcx_t f, mpcx_t g) {
   fprintf (stderr, "Error in add: f + g yields h1 and h2 with\nf: ");
   mpcx_out_str (stderr, 10, 0, f);
   fprintf (stderr, "\ng: ");
   mpcx_out_str (stderr, 10, 0, g);
   fprintf (stderr, "\nh1: ");
   mpcx_out_str (stderr, 10, 0, h1);
   fprintf (stderr, "\nh2: ");
   mpcx_out_str (stderr, 10, 0, h2);
   fprintf (stderr, "\n");
   exit (1);
}


static void check_add (mpcx_t f, mpcx_t g) {
   /* checks whether addition in place works; f and g must have the
      same precision */
   mpcx_t f2, g2, h;

   mpcx_init_set (f2, f);
   mpcx_init_set (g2, g);
   mpcx_init (h, 10, mpcx_get_prec (f));

   mpcx_add (h, f, g);

   /* check for difference between normal and in place operation */
   mpcx_add (f2, f2, g);
   if (mpcx_cmp (h, f2))
      error (h, f2, f, g);

   /* check for difference between normal and in place operation */
   mpcx_add (g2, f, g2);
   if (mpcx_cmp (h, g2))
      error (h, g2, f, g);

   /* check whether f+g == f - (-g) */
   mpcx_neg (g2, g);
   mpcx_sub (f2, f, g2);
   if (mpcx_cmp (h, f2))
      error (h, f2, f, g);

   /* check whether f+g = g+f; should hold even in floating point */
   mpcx_add (f2, g, f);
   if (mpcx_cmp (h, f2))
      error (h, f2, f, g);

   mpcx_clear (f2);
   mpcx_clear (g2);
   mpcx_clear (h);
}


static void check_add_random (gmp_randstate_t state) {
   int deg;
   mpcx_t f, g;

   mpcx_init (f, 10, 103);
   mpcx_init (g, 11, 103);

   for (deg = -1; deg <= 1000; deg += 10) {
      mpcx_urandom (f, deg, state);
      mpcx_urandom (g, deg, state);
      check_add (f, g);
      check_add (f, f);
   }

   mpcx_clear (f);
   mpcx_clear (g);
}


int main (void) {
   gmp_randstate_t state;

   gmp_randinit_default (state);

   check_add_random (state);

   gmp_randclear (state);

   return 0;
}
