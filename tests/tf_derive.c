/*
test file for derive

Copyright (C) 2017 Andreas Enge

This file is part of the MPFRCX Library.

The MPFRCX Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The MPFRCX Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MPFRCX library; see the file COPYING.LESSER.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
*/

#include "mpfrcx.h"

int main (void)
{
   mpfrx_t f;
   int ok;

   mpfrx_init (f, 3, 30);
   mpfrx_set_deg (f, 2);
   mpfr_set_ui (mpfrx_get_coeff (f, 2), 1, GMP_RNDN);
   mpfr_set_ui (mpfrx_get_coeff (f, 1), 17, GMP_RNDN);
   mpfr_set_ui (mpfrx_get_coeff (f, 0), 42, GMP_RNDN);

   mpfrx_derive (f, f);

   ok = mpfrx_get_deg (f) == 1
       && !mpfr_cmp_si (mpfrx_get_coeff (f, 1), 2)
       && !mpfr_cmp_si (mpfrx_get_coeff (f, 0), 17);

   mpfrx_clear (f);

   return (!ok);
}

