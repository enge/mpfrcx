/*
test file for derive

Copyright (C) 2017 Andreas Enge

This file is part of the MPFRCX Library.

The MPFRCX Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The MPFRCX Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MPFRCX library; see the file COPYING.LESSER.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
*/

#include "mpfrcx.h"

int main (void)
{
   mpcx_t f;
   int ok;

   mpcx_init (f, 3, 30);
   mpcx_set_deg (f, 2);
   mpc_set_ui (mpcx_get_coeff (f, 2), 1, MPC_RNDNN);
   mpc_set_ui (mpcx_get_coeff (f, 1), 17, MPC_RNDNN);
   mpc_set_ui (mpcx_get_coeff (f, 0), 42, MPC_RNDNN);

   mpcx_derive (f, f);

   ok = mpcx_get_deg (f) == 1
       && !mpc_cmp_si (mpcx_get_coeff (f, 1), 2)
       && !mpc_cmp_si (mpcx_get_coeff (f, 0), 17);

   mpcx_clear (f);

   return (!ok);
}

