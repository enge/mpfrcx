/*
test file for eval

Copyright (C) 2020 Andreas Enge

This file is part of the MPFRCX Library.

The MPFRCX Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The MPFRCX Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MPFRCX library; see the file COPYING.LESSER.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
*/

#include <stdio.h>
#include "mpfrcx.h"


static void check_eval (mpfrx_t f, mpfr_t x) {
   /* checks whether evaluation in place works; assumes that the real and
      imaginary parts of x have the same precision; replaces x by f(x) */
   mpfr_t r;

   mpfr_init2 (r, mpfr_get_prec (x));

   /* check for difference between normal and in place operation */
   mpfrx_eval (r, f, x);
   mpfrx_eval (x, f, x);
   if (mpfr_cmp (r, x)) {
      fprintf (stderr, "Error in eval: f(x) yields different values\nf: ");
      mpfrx_out_str (stderr, 10, 0, f);
      fprintf (stderr, "\nout of place: ");
      mpfr_out_str (stderr, 10, 0, r, GMP_RNDN);
      fprintf (stderr, "\nin place:     ");
      mpfr_out_str (stderr, 10, 0, x, GMP_RNDN);
      fprintf (stderr, "\n");
      exit (1);
   }

   mpfr_clear (r);
}


static void check_eval_random (gmp_randstate_t state) {
   int deg;
   mpfrx_t f;
   mpfr_t x;

   mpfrx_init (f, 10, 103);
   mpfr_init2 (x, 103);

   for (deg = -1; deg <= 1000; deg += 200) {
      mpfrx_urandom (f, deg, state);
      mpfr_urandom (x, state, GMP_RNDN);
      check_eval (f, x);
   }

   mpfrx_clear (f);
   mpfr_clear (x);
}


int main (void) {
   gmp_randstate_t state;

   gmp_randinit_default (state);

   check_eval_random (state);

   return 0;
}
