/*
test file for eval

Copyright (C) 2020 Andreas Enge

This file is part of the MPFRCX Library.

The MPFRCX Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The MPFRCX Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MPFRCX library; see the file COPYING.LESSER.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
*/

#include <stdio.h>
#include "mpfrcx.h"


static void check_eval (mpcx_t f, mpc_t x) {
   /* checks whether evaluation in place works; assumes that the real and
      imaginary parts of x have the same precision; replaces x by f(x) */
   mpc_t r;

   mpc_init2 (r, mpc_get_prec (x));

   /* check for difference between normal and in place operation */
   mpcx_eval (r, f, x);
   mpcx_eval (x, f, x);
   if (mpc_cmp (r, x)) {
      fprintf (stderr, "Error in eval: f(x) yields different values\nf: ");
      mpcx_out_str (stderr, 10, 0, f);
      fprintf (stderr, "\nout of place: ");
      mpc_out_str (stderr, 10, 0, r, MPC_RNDNN);
      fprintf (stderr, "\nin place:     ");
      mpc_out_str (stderr, 10, 0, x, MPC_RNDNN);
      fprintf (stderr, "\n");
      exit (1);
   }

   mpc_clear (r);
}


static void check_eval_random (gmp_randstate_t state) {
   int deg;
   mpcx_t f;
   mpc_t x;

   mpcx_init (f, 10, 103);
   mpc_init2 (x, 103);

   for (deg = -1; deg <= 1000; deg += 200) {
      mpcx_urandom (f, deg, state);
      mpc_urandom (x, state);
      check_eval (f, x);
   }

   mpcx_clear (f);
   mpc_clear (x);
}


static void check_example () {
   mpcx_t f;
   mpc_t x, r;
   mpfr_prec_t p;
   int i;

   p = 11;
   mpcx_init (f, 5, p);
   mpc_init2 (x, p);
   mpc_init2 (r, p);

   mpcx_set_deg (f, 4);
   for (i = 4; i > 0; i--)
      mpc_set_ui_ui (mpcx_get_coeff (f, i), i, i, MPC_RNDNN);
   mpc_set_ui_ui (mpcx_get_coeff (f, 0), 2, 4, MPC_RNDNN);
   mpc_set_ui_ui (x, 3, 4, MPC_RNDNN);

   mpcx_eval (r, f, x);

   if (   mpfr_cmp_si (mpc_realref (r), -1308)
       || mpfr_cmp_si (mpc_imagref (r), -3626)) {
      fprintf (stderr, "Error in eval:\nf: ");
      mpcx_out_str (stderr, 10, 0, f);
      fprintf (stderr, "\nx: ");
      mpc_out_str (stderr, 10, 0, x, MPC_RNDNN);
      fprintf (stderr, "\nexpected: (-1308, -3626)\ngot:      ");
      mpc_out_str (stderr, 10, 0, r, MPC_RNDNN);
      exit (1);
   }

   mpcx_clear (f);
   mpc_clear (x);
   mpc_clear (r);
}

int main (void) {
   gmp_randstate_t state;

   gmp_randinit_default (state);

   check_eval_random (state);

   check_example  ();

   return 0;
}
