/*
test file for tree functions

Copyright (C) 2018, 2021 Andreas Enge

This file is part of the MPFRCX Library.

The MPFRCX Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The MPFRCX Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MPFRCX library; see the file COPYING.LESSER.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
*/

#include "mpfrcx.h"

/**************************************************************************/

static int real_get_si (long int *z, mpfr_srcptr f)
   /* Round the real to an integer value; the return value reflects the
      success of the operation, in the sense that the difference is
      sufficiently small. */

{
   mpfr_t diff;
   mpfr_prec_t prec;
   mpfr_exp_t e;
   int ok;

   prec = mpfr_get_prec (f);
   e = mpfr_get_exp (f);
   if (prec <= e)
      /* The number is trivially an integer, but with no fractional part,
         so the rounding error has become invisible. */
      return (0);

   mpfr_init2 (diff, 2);
   *z = mpfr_get_si (f, MPFR_RNDN);
   mpfr_sub_si (diff, f, *z, MPFR_RNDN);

   ok = (mpfr_sgn (diff) == 0 || mpfr_get_exp (diff) <= -10);
      /* We arbitrarily accept a rounding error of at most 2^(-10). */

   mpfr_clear (diff);

   return (ok);
}

/**************************************************************************/

static int example1 (void) {
   /* The example comes from a Weber class invariant for D=-1820 as
      described in Jared Asuncion's master thesis. */

   int i, j, k;
   mpfr_prec_t prec = 128;
   mpc_t *roots;
   int *conjugates;
   int deg = 20, d [3] = { 5, 2, 2 }, no = 12;
   int ok;
   mpfrx_tower_t twr;
   mpfr_ptr coeff;
   long int z;
   const long int W [3][3][10]
      = {{{ -73, -80, -74, -39, -6, 1, 0, 0, 0, 0},
          { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
          { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}},
         {{ -140, 53, 54, 12, 1, 0, 0, 0, 0, 0},
          { 365, 320, 222, 78, 6, 0, 0, 0, 0, 0},
          { -80, -148, -117, -24, 5, 0, 0, 0, 0, 0}},
         {{ -95, 55, 228, -250, -47, 160, 20, -142, 80, -12},
          { -110, -279, 1224, -1414, 624, -60, 116, -186, 76, -6},
          { 31, -306, 606, -416, 60, -174, 434, -304, 54, 10 }}};

   roots = (mpc_t *) malloc (deg * sizeof (mpc_t));
   for (i = 0; i < no; i++)
      mpc_init2 (roots [i], prec);
   conjugates = (int *) malloc (deg * sizeof (int));
   for (i = 0; i < 4; i++)
      conjugates [i] = i;
   conjugates [ 4] = 18;
   conjugates [ 5] = 19;
   conjugates [ 6] = 16;
   conjugates [ 7] = 17;
   conjugates [ 8] = 14;
   conjugates [ 9] = 15;
   conjugates [10] = 12;
   conjugates [11] = 13;
   conjugates [12] = 10;
   conjugates [13] = 11;
   conjugates [14] =  8;
   conjugates [15] =  9;
   conjugates [16] =  6;
   conjugates [17] =  7;
   conjugates [18] =  4;
   conjugates [19] =  5;

   mpc_set_str (roots [ 0], "11.537612002601293729555643926712700128", 10, MPC_RNDNN);
   mpc_set_str (roots [ 1], "-1.2359811407720793940569330632262337764", 10, MPC_RNDNN);
   mpc_set_str (roots [ 2], "-0.88158412095480496703824738845507654109", 10, MPC_RNDNN);
   mpc_set_str (roots [ 3], "1.0537757586472475214611033030981796170", 10, MPC_RNDNN);
   mpc_set_str (roots [ 4], "(-1.3738815073324657798749662481447867991 -1.1528234664875328372707537955648321806)", 10, MPC_RNDNN);
   mpc_set_str (roots [ 5], "(0.15539825039129532339792538912105083438 0.83255682211942350580400245155255066129)", 10, MPC_RNDNN);
   mpc_set_str (roots [ 6], "(0.23751391794267422186896086399600757039 -0.66425231309624577378237568506293771302)", 10, MPC_RNDNN);
   mpc_set_str (roots [ 7], "(0.75323765599341107222341710074633201727 -0.24354073806061626168053242233604424063)", 10, MPC_RNDNN);
   mpc_set_str (roots [ 8], "(-0.66231668017784407796844395136033368855 0.70148728032360712487311562970674213590)", 10, MPC_RNDNN);
   mpc_set_str (roots [ 9], "(-0.62514835842316383925952894202832472183 -0.49160598090266240182413906785025006010)", 10, MPC_RNDNN);
   mpc_set_str (roots [10], "(-0.22655403352361409872463829988445090319 -0.81762769695565820067029579086297358680)", 10, MPC_RNDNN);
   mpc_set_str (roots [11], "(-0.49516049463112126662350930151027902325 -0.048769033268922442525849010844560810424)", 10, MPC_RNDNN);

   mpfrx_tower_init (twr, 3, d, prec);
   mpfrcx_tower_decomposition (twr, roots, conjugates);

   /* Check the degrees. */
   ok = mpfrx_get_deg (twr->W [0][0]) == 5;
   for (j = 0; j < 3; j++)
      ok = ok && mpfrx_get_deg (twr->W [1][j]) == 4;
   for (j = 0; j < 3; j++)
      ok = ok && mpfrx_get_deg (twr->W [2][j]) == 9;

   /* Check whether the result is the correct integral polynomial. */
   for (i = 0; i < 3; i++)
      for (j = 0; j <= (i == 0 ? 0 : d [i]); j++)
         for (k = 0; k <= mpfrx_get_deg (twr->W [i][j]); k++) {
            coeff = mpfrx_get_coeff (twr->W [i][j], k);
            ok = ok && real_get_si (&z, coeff)
                    && z == W [i][j][k];
      }

   mpfrx_tower_clear (twr);

   for (i = 0; i < no; i++)
      mpc_clear (roots [i]);
   free (roots);
   free (conjugates);

   return (ok);
}

/**************************************************************************/

static int example2 (void) {
   /* only one level */

   int i, k;
   mpfr_prec_t prec = 128;
   mpc_t *roots;
   int *conjugates;
   int deg = 3, d [1] = { 3 }, no = 2;
   int ok;
   mpfrx_tower_t twr;
   mpfr_ptr coeff;
   long int z;
   const long int W [4] = { -2, 0, 0, 1 };

   roots = (mpc_t *) malloc (deg * sizeof (mpc_t));
   for (i = 0; i < no; i++)
      mpc_init2 (roots [i], prec);
   conjugates = (int *) malloc (deg * sizeof (int));
   conjugates [0] = 0;
   conjugates [1] = 2;
   conjugates [2] = 1;

   mpc_set_str (roots [ 0], "1.2599210498948731647672106072782283506", 10, MPC_RNDNN);
   mpc_set_str (roots [ 1], "(-0.62996052494743658238360530363911417528 1.0911236359717214035600726141898088813)", 10, MPC_RNDNN);

   mpfrx_tower_init (twr, 1, d, prec);
   mpfrcx_tower_decomposition (twr, roots, conjugates);

   /* Check whether the result is the correct integral polynomial. */
   ok = 1;
   for (k = 0; k < 4; k++) {
      coeff = mpfrx_get_coeff (twr->W [0][0], k);
      ok = ok && real_get_si (&z, coeff)
         && z == W [k];
   }

   mpfrx_tower_clear (twr);

   for (i = 0; i < no; i++)
      mpc_clear (roots [i]);
   free (roots);
   free (conjugates);

   return (ok);
}

/**************************************************************************/

static int example3 (void) {
   /* The example comes from the double eta class invariant of level
      2*73 for D=-1087, which exposed a bug in release 0.6. */

   int i, j, k;
   mpfr_prec_t prec = 19;
   mpc_t *roots;
   int *conjugates;
   int deg = 9, d [2] = { 3, 3 };
   int ok;
   mpfrx_tower_t twr;
   mpfr_ptr coeff;
   long int z;
   const long int W [2][4][4]
      = {{{ -11, 4, -9, 1},
          { 0, 0, 0, 0},
          { 0, 0, 0, 0},
          { 0, 0, 0, 0}},
         {{ 42, -12, 31, 0},
          { -19, 18, 32, 0},
          { 33, -8, 9, 0},
          { 4, -18, 3, 0}}};

   roots = (mpc_t *) malloc (6 * sizeof (mpc_t));
   mpc_init2 (roots [0], prec);
   for (i = 2; i < 6; i++)
      mpc_init2 (roots [i], prec);
   conjugates = (int *) malloc (deg * sizeof (int));
   conjugates [0] = 1;
   conjugates [1] = 0;
   conjugates [2] = 2;
   conjugates [3] = 6;
   conjugates [4] = 8;
   conjugates [5] = 7;
   conjugates [6] = 3;
   conjugates [7] = 5;
   conjugates [8] = 4;

   mpc_set_str (roots [0], "(-3.738129 3.388008)", 10, MPC_RNDNN);
   mpc_set_str (roots [2], "-1.209091", 10, MPC_RNDNN);
   mpc_set_str (roots [3], "(5.316985e-2 1.612884e-2)", 10, MPC_RNDNN);
   mpc_set_str (roots [4], "(-1.551620 1.628727)", 10, MPC_RNDNN);
   mpc_set_str (roots [5], "(1.341080 -5.305252e-1)", 10, MPC_RNDNN);

   mpfrx_tower_init (twr, 2, d, prec);
   mpfrcx_tower_decomposition (twr, roots, conjugates);

   /* Check the degrees. */
   ok = mpfrx_get_deg (twr->W [0][0]) == 3;
   for (j = 0; j <= 3; j++)
      ok = ok && mpfrx_get_deg (twr->W [1][j]) == 2;

   /* Check whether the result is the correct integral polynomial. */
   for (i = 0; i < 2; i++)
      for (j = 0; j <= (i == 0 ? 0 : d [i]); j++)
         for (k = 0; k <= mpfrx_get_deg (twr->W [i][j]); k++) {
            coeff = mpfrx_get_coeff (twr->W [i][j], k);
            ok = ok && real_get_si (&z, coeff)
                    && z == W [i][j][k];
      }

   mpfrx_tower_clear (twr);

   mpc_clear (roots [0]);
   for (i = 2; i < 6; i++)
      mpc_clear (roots [i]);
   free (roots);
   free (conjugates);

   return (ok);
}

/**************************************************************************/

int main (void){
   return !(example1 () && example2 () && example3 ());
}

