/*
test file for get_version

Copyright (C) 2010, 2011 Andreas Enge

This file is part of the MPFRCX Library.

The MPFRCX Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The MPFRCX Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MPFRCX library; see the file COPYING.LESSER.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "config.h"
#include "mpfrcx.h"

int
main (void)
{
#ifdef __MPIR_VERSION
   printf ("MPIR:   include %d.%d.%d, lib %s\n",
           __MPIR_VERSION, __MPIR_VERSION_MINOR, __MPIR_VERSION_PATCHLEVEL,
           mpir_version);
#else
   printf ("GMP:    include %d.%d.%d, lib %s\n",
           __GNU_MP_VERSION, __GNU_MP_VERSION_MINOR, __GNU_MP_VERSION_PATCHLEVEL,
           gmp_version);
#endif
   printf ("MPFR:   include %s, lib %s\n",
           MPFR_VERSION_STRING,
           mpfr_get_version ());
   printf ("MPC:    include %s, lib %s\n", MPC_VERSION_STRING,
           mpc_get_version ());
   printf ("MPFRCX: include %s, lib %s\n", MPFRCX_VERSION_STRING,
           mpfrcx_get_version ());

   if (strcmp (mpfrcx_get_version (), MPFRCX_VERSION_STRING) != 0) {
      printf ("Error: header and library do not match\n");
      exit (1);
   }

#ifdef MPFRCX_CC
  printf ("C compiler:  %s\n", MPFRCX_CC);
#endif
#ifdef MPFRCX_GCC
  printf ("GCC:         %s\n", MPFRCX_GCC);
#endif
#ifdef MPFRCX_GCC_VERSION
  printf ("GCC version: %s\n", MPFRCX_GCC_VERSION);
#endif

   return 0;
}
