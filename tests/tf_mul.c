/*
test file for mul

Copyright (C) 2009, 2010, 2013 Andreas Enge

This file is part of the MPFRCX Library.

The MPFRCX Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 3 of the License, or (at your
option) any later version.

The MPFRCX Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the MPFRCX library; see the file COPYING.LESSER.  If not, write to
the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
MA 02111-1307, USA.
*/

#include <stdio.h>
#include "mpfrcx.h"


static void error (mpfrx_t h1, mpfrx_t h2, mpfrx_t f, mpfrx_t g) {
   fprintf (stderr, "Error in mul: f * g yields h1 and h2 with\nf: ");
   mpfrx_out_str (stderr, 10, 0, f);
   fprintf (stderr, "\ng: ");
   mpfrx_out_str (stderr, 10, 0, g);
   fprintf (stderr, "\nh1: ");
   mpfrx_out_str (stderr, 10, 0, h1);
   fprintf (stderr, "\nh2: ");
   mpfrx_out_str (stderr, 10, 0, h2);
   fprintf (stderr, "\n");
   exit (1);
}


static void check_mul (mpfrx_t f, mpfrx_t g) {
   /* checks whether multiplication in place works; f and g must have the
      same precision */
   mpfrx_t f2, g2, h;

   mpfrx_init_set (f2, f);
   mpfrx_init_set (g2, g);
   mpfrx_init (h, 10, mpfrx_get_prec (f));

   mpfrx_mul (h, f, g);

   /* check for difference between normal and in place operation */
   mpfrx_mul (f2, f2, g);
   if (mpfrx_cmp (h, f2))
      error (h, f2, f, g);

   /* check for difference between normal and in place operation */
   mpfrx_mul (g2, f, g2);
   if (mpfrx_cmp (h, g2))
      error (h, g2, f, g);

   /* check whether f*g = g*f; should hold in floating point at least
      when using the fft */
   mpfrx_mul (f2, g, f);
   if (mpfrx_cmp (h, f2))
      error (h, f2, f, g);

   mpfrx_clear (f2);
   mpfrx_clear (g2);
   mpfrx_clear (h);
}


static void check_mul_random (gmp_randstate_t state) {
   int deg;
   mpfrx_t f, g;

   mpfrx_init (f, 10, 103);
   mpfrx_init (g, 11, 103);

   for (deg = -1; deg <= 1000; deg += 200) {
      mpfrx_urandom (f, deg, state);
      mpfrx_urandom (g, deg, state);
      check_mul (f, g);
      check_mul (f, f);
   }

   mpfrx_clear (f);
   mpfrx_clear (g);
}


static void check_unbalanced_mul () {
   const int degf = 4000, degg = 1000;
   const mpfr_prec_t prec = 200;
   mpfrx_t f, g, h;
   mpfr_t coeff;
   unsigned long int coeff_int;
   int i;

   mpfrx_init (f, degf + 1, prec);
   mpfrx_init (g, degg + 1, prec);
   mpfrx_init (h, degf + degg + 1, prec);
   mpfr_init2 (coeff, prec);

   mpfrx_set_deg (f, degf);
   mpfrx_set_deg (g, degg);
   for (i = 0; i <= degf; i++)
      mpfr_set_ui (mpfrx_get_coeff (f, i), i, GMP_RNDN);
   for (i = 0; i <= degg; i++)
      mpfr_set_ui (mpfrx_get_coeff (g, i), degg + 1 - i, GMP_RNDN);
   mpfrx_mul (h, f, g);
   /* largest coefficient is that of x^4000, 1838837000 */
   mpfr_abs (coeff, mpfrx_get_coeff (h, degf), GMP_RNDN);
   coeff_int = mpfr_get_ui (coeff, GMP_RNDN);

   if (coeff_int != 1838837000)
      exit (1);

   mpfrx_clear (f);
   mpfrx_clear (g);
   mpfrx_clear (h);
   mpfr_clear (coeff);
}


int main (void) {
   gmp_randstate_t state;

   gmp_randinit_default (state);

   check_mul_random (state);

   gmp_randclear (state);

   check_unbalanced_mul ();

   return 0;
}
